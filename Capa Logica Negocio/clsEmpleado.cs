﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using CapaEnlaceDatos;
using System.Data.SqlClient;

namespace CapaLogicaNegocio
{
    public class clsEmpleado
    {
        clsManejador M = new clsManejador();
        int vv = 0;
        public int Clave { get; set; }
        public string Especialidad { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Sexo { get; set; }
        public string FechaCon { get; set; }
        public string Telefono { get; set; }
        public byte[] Imagen { get; set; }
        public string Correo { get; set; }
        public string Direccion { get; set; }
        public string Edad { get; set; }
        public string nombredoc { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string obs { get; set; }
        public int VariableAcceso { get; set; }

        public String RegistrarDoctor()
        {
            List<clsParametro> lst = new List<clsParametro>();
            String Mensaje = "";
            try
            {
                lst.Add(new clsParametro("@Clave",Clave));
                lst.Add(new clsParametro("@Nombre",Nombres));
                lst.Add(new clsParametro("@Direccion",Direccion));
                lst.Add(new clsParametro("@Telefono",Telefono));
                lst.Add(new clsParametro("@Especialidad",Especialidad));
                lst.Add(new clsParametro("@Correo",Correo));
                lst.Add(new clsParametro("@idrecepcionista",1));
                lst.Add(new clsParametro("@idadministrador",1));
                lst.Add(new clsParametro("@Acceso_us", 2));
                lst.Add(new clsParametro("@Usuario",User));
                lst.Add(new clsParametro("@contraseña",Password));
               
                lst.Add(new clsParametro("@Mensaje", "", SqlDbType.VarChar, ParameterDirection.Output, 100));
                M.EjecutarSP("RegistrarDoctores", ref lst);
                return Mensaje = lst[11].Valor.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public String ActualizarDoctor()
        {
            List<clsParametro> lst = new List<clsParametro>();
            String Mensaje = "";
            try
            {
                lst.Add(new clsParametro("@Clave", Clave));
                lst.Add(new clsParametro("@Nombre", Nombres));
                lst.Add(new clsParametro("@Direccion", Direccion));
                lst.Add(new clsParametro("@Telefono", Telefono));
                lst.Add(new clsParametro("@Especialidad", Especialidad));
                lst.Add(new clsParametro("@Correo", Correo));
                lst.Add(new clsParametro("@idrecepcionista",1));
                lst.Add(new clsParametro("@idadministrador",1));
                lst.Add(new clsParametro("@Acceso_us", 2));
                lst.Add(new clsParametro("@Usuario", User));
                lst.Add(new clsParametro("@contraseña", Password));
                lst.Add(new clsParametro("@Mensaje", "", SqlDbType.VarChar, ParameterDirection.Output, 100));
                M.EjecutarSP("ActualizarDoctores", ref lst);
                return Mensaje = lst[11].Valor.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String RegistrarRecepcionista()
        {
            List<clsParametro> lst = new List<clsParametro>();
            String Mensaje = "";
            try
            {
                lst.Add(new clsParametro("@Clave", Clave));
                lst.Add(new clsParametro("@Nombre", Nombres));
                lst.Add(new clsParametro("@Direccion", Direccion));
                lst.Add(new clsParametro("@Telefono", Telefono));
                lst.Add(new clsParametro("@Correo", Correo));
                lst.Add(new clsParametro("@iddoctor", 1));
                lst.Add(new clsParametro("@idadministrador", 1));
                lst.Add(new clsParametro("@Acceso_us", 1));
                lst.Add(new clsParametro("@Usuario", User));
                lst.Add(new clsParametro("@contraseña", Password));
                
                lst.Add(new clsParametro("@Mensaje", "", SqlDbType.VarChar, ParameterDirection.Output, 100));
                M.EjecutarSP("RegistrarRecepcionista", ref lst);
                return Mensaje = lst[10].Valor.ToString();



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public String ActualizarRecepcionista()
        {
            List<clsParametro> lst = new List<clsParametro>();
            String Mensaje = "";
            try
            {
                lst.Add(new clsParametro("@Clave", Clave));
                lst.Add(new clsParametro("@Nombre", Nombres));
                lst.Add(new clsParametro("@Direccion", Direccion));
                lst.Add(new clsParametro("@Telefono", Telefono));
                lst.Add(new clsParametro("@Correo", Correo));
                lst.Add(new clsParametro("@iddoctor", 1));
                lst.Add(new clsParametro("@idadministrador", 1));
                lst.Add(new clsParametro("@Acceso_us", 1));
                lst.Add(new clsParametro("@Usuario", User));
                lst.Add(new clsParametro("@contraseña", Password));
                
                lst.Add(new clsParametro("@Mensaje", "", SqlDbType.VarChar, ParameterDirection.Output, 100));
                M.EjecutarSP("ActualizarRecepcionista", ref lst);
                return Mensaje = lst[10].Valor.ToString();



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String RegistrarPaciente()
        {
            List<clsParametro> lst = new List<clsParametro>();
            String Mensaje = "";
            try
            {
                lst.Add(new clsParametro("@Clave", Clave));
                lst.Add(new clsParametro("@Nombre", Nombres));
                lst.Add(new clsParametro("@Apellidos", Apellidos));
                lst.Add(new clsParametro("@Direccion", Direccion));
                lst.Add(new clsParametro("@Telefono", Telefono));
                lst.Add(new clsParametro("@Edad", Edad));
                lst.Add(new clsParametro("@Correo", Correo));
                lst.Add(new clsParametro("@Sexo", Sexo));
                lst.Add(new clsParametro("@Fotopaciente", Imagen));
                lst.Add(new clsParametro("@Fechaconsulta", FechaCon));
                lst.Add(new clsParametro("@NombreDoctor", nombredoc));
                lst.Add(new clsParametro("@Mensaje", "", SqlDbType.VarChar, ParameterDirection.Output, 100));
                M.EjecutarSP("Registrarpaciente", ref lst);
                return Mensaje = lst[11].Valor.ToString();

            }
            catch (Exception ex   )        {
                throw ex;
            }
        }
        public String RegistrarExp()
        {
            List<clsParametro> lst = new List<clsParametro>();
            String Mensaje = "";
            try
            {
                lst.Add(new clsParametro("@Clave", Clave));
                lst.Add(new clsParametro("@Antecedentes", Nombres));
                lst.Add(new clsParametro("@Peso", Apellidos));
                lst.Add(new clsParametro("@Altura", Direccion));
                lst.Add(new clsParametro("@Temperatura", Telefono));
                lst.Add(new clsParametro("@PresionArterial", Edad));
                lst.Add(new clsParametro("@FrecuenciaCardiaca", Correo));
                lst.Add(new clsParametro("@Motivodeconsulta", Sexo));
                lst.Add(new clsParametro("@Alergias", FechaCon));
                lst.Add(new clsParametro("@Sintomas", nombredoc));
                lst.Add(new clsParametro("@Observaciones", obs));
                lst.Add(new clsParametro("@Mensaje", "", SqlDbType.VarChar, ParameterDirection.Output, 100));
                M.EjecutarSP("RegistrarExp", ref lst);
                return Mensaje = lst[11].Valor.ToString();

            }
            catch (Exception ex   )        {
                throw ex;
            }
        }
        public String ActualizarPaciente()
        {
            List<clsParametro> lst = new List<clsParametro>();
            String Mensaje = "";
            try
            {
                lst.Add(new clsParametro("@Clave", Clave));
                lst.Add(new clsParametro("@Nombre", Nombres));
                lst.Add(new clsParametro("@Apellidos", Apellidos));
                lst.Add(new clsParametro("@Direccion", Direccion));
                lst.Add(new clsParametro("@Telefono", Telefono));
                lst.Add(new clsParametro("@Edad", Edad));
                lst.Add(new clsParametro("@Correo", Correo));
                lst.Add(new clsParametro("@Sexo", Sexo));
                lst.Add(new clsParametro("@FotoPac", Imagen));
                lst.Add(new clsParametro("@Fechaconsulta", FechaCon));
                lst.Add(new clsParametro("@Mensaje", "", SqlDbType.VarChar, ParameterDirection.Output, 100));
                M.EjecutarSP("ActualizarPacientes", ref lst);
                return Mensaje = lst[10].Valor.ToString();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable ListadoEmpleados()
        {
            return M.Listado("ListadoEmpleados", null);
        }

        public String GenerarIdEmpleado()
        {
            List<clsParametro> lst = new List<clsParametro>();
            int objIdEmpleado;
            try
            {
                lst.Add(new clsParametro("@IdEmpleado", "", SqlDbType.Int, ParameterDirection.Output, 4));
                M.EjecutarSP("GenerarIdEmpleado", ref lst);
                objIdEmpleado = Convert.ToInt32(lst[0].Valor.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Convert.ToString(objIdEmpleado);
        }

        public DataTable BuscarEmpleado(String objDatos)
        {
            DataTable dt = new DataTable();
            List<clsParametro> lst = new List<clsParametro>();
            lst.Add(new clsParametro("@Datos", objDatos));
            return dt = M.Listado("Buscar_Empleado", lst);
        }

        public String EliminarEmpleado()
        {
            List<clsParametro> lst = new List<clsParametro>();
            String Mensaje = "";
            try
            {
                lst.Add(new clsParametro("@IdEmpleado", Clave));
                M.EjecutarSP("EliminarEmpleado", ref lst);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Mensaje;
        }
    

    }
}
