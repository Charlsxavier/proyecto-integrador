﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using CapaEnlaceDatos;

namespace CapaLogicaNegocio
{
    public class clsUsuarios
    {
        clsManejador M = new clsManejador();

        public int IdEmpleado { get; set; }
        public string User { get; set; }
        public string Password { get; set; }

        public String RegistrarDoctor()
        {
            int Iddoctor = 1;
            int Idrecepcionista = 2;
            int Idadministrador = 3;
            List<clsParametro> lst = new List<clsParametro>();
            String Mensaje = "";
            try
            {
                lst.Add(new clsParametro("@Iddoctor", Iddoctor));
                lst.Add(new clsParametro("@Idrecepcionista", Idrecepcionista));
                lst.Add(new clsParametro("@Idadministrador", Idadministrador));
                lst.Add(new clsParametro("@Usuario", User));
                lst.Add(new clsParametro("@Contraseña", Password));
                lst.Add(new clsParametro("@Mensaje", "", SqlDbType.VarChar, ParameterDirection.Output, 50));
                M.EjecutarSP("RegistrarDoctor", ref lst);
                return Mensaje = lst[5].Valor.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String IniciarSesion()
        {
            int VariabledeAcceso = 2;
            List<clsParametro> lst = new List<clsParametro>();
            String Mensaje = "";
            try
            {
                lst.Add(new clsParametro("@Usuario", User));
                lst.Add(new clsParametro("@Contraseña", Password));
                lst.Add(new clsParametro("@VariabledeAcceso", VariabledeAcceso));
                lst.Add(new clsParametro("@Mensaje", "", SqlDbType.VarChar, ParameterDirection.Output, 50));

                M.EjecutarSP("IniciarSesion", ref lst);
                return Mensaje = lst[3].Valor.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable DevolverDatosSesion(String objUser, String objPassword)
        {
            List<clsParametro> lst = new List<clsParametro>();
            try
            {
                lst.Add(new clsParametro("@Usuario", objUser));
                lst.Add(new clsParametro("@Contraseña", objPassword));
                return M.Listado("DevolverDatosSesion", lst);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
