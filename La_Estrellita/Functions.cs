﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.Windows.Forms;
using ImageMagick;
using System.Data;
using System.Data.SqlClient;
using CapaEnlaceDatos;
using CapaLogicaNegocio;

namespace La_Estrellita
{

    public class Functions : Ivariables
    {
        clsEmpleado emp = new clsEmpleado();
        clsManejador man = new clsManejador();
        public List<String> PatientTableColumnNames = new List<string> { "Id", "Clave", "Nombre", "Apellido" ,"Direccion","telefono","edad",
                                                                          "sexo","correo","fotopaciente","fechaconsulta","nombredoctor"};
        public List<String> PresTableColumnNames = new List<string> { "Medicamento", "Dosis", "Frequencia" };

        public List<String> ExpTableColumnNames = new List<string> { "Clave", "Antecedentes", "Peso", "Altura", "Temperatura", "Presion Arterial", 
                                                                    "Frecuencia Cardiaca", "Motivo de consulta", "Alergias", "Sintomas", "Observaciones" };
        public string filePath;
        public string imagePath;
        string tempPath = Path.GetTempPath();
        iTextSharp.text.Font bold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 14);
        iTextSharp.text.Font small = FontFactory.GetFont(FontFactory.HELVETICA, 10);
        public string dialogFile { get; set; }
        public string patientGetMeds { get; set; }
        public string Nombre_pac { get; set; }
        public string Sexo_pac { get; set; }
        public string Edad_pac { get; set; }
        public string Clave_pac { get; set; }
        public string NombreDoc_pac { get; set; }
        public byte[] Imagen { get; set; }
        public Paragraph CreateLine(string title, string patientInfo)
        {
            Chunk label = new Chunk(title + " ", bold);
            Chunk patientFillInfo = new Chunk(patientInfo);
            Phrase fullLine = new Phrase();
            fullLine.Add(label);
            fullLine.Add(patientFillInfo);
            Paragraph compoundLine = new Paragraph();
            compoundLine.Add(fullLine);
            compoundLine.SpacingAfter = 10.5f;
            return compoundLine;
        }


        public void AddItemsToCombo(string command, ComboBox box)
        {
            man.Conectar();
            using (var comando = new SqlCommand(command, man.conexion))
            {
                using (var reader = comando.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            box.Items.Add(reader.GetString(i));
                        }
                    }
                }

            }

        }
        public Paragraph CreateLineCentered(string text)
        {
            Chunk label = new Chunk(text + " ", small);
            Phrase fullLine = new Phrase();
            fullLine.Add(label);
            Paragraph compoundLine = new Paragraph();
            compoundLine.Add(fullLine);
            compoundLine.SpacingAfter = 2.5f;
            compoundLine.Alignment = Element.ALIGN_CENTER;
            return compoundLine;
        }
        public string LineBreaker(string sentence, int limit)
        {
            int counter = sentence.Length;
            string firstHalf = "";
            string secondHalf = "";
            if (sentence.Length > limit)
            {
                string[] splitSentence = sentence.Split(' ');
                for (int x = limit; x > 0; x--)
                {
                    if (sentence[x] == ' ')
                    {
                        firstHalf = sentence.Substring(0, x);
                        break;
                    }
                }
                secondHalf = "\n" + sentence.Substring(firstHalf.Length + 1);
            }
            else { firstHalf = sentence; }
            sentence = firstHalf + secondHalf;
            return sentence;
        }
        public Paragraph SeveralItemAdd(string[] title, string[] content, float spaceBefore, float spaceAfter)
        {
            Paragraph compoundLine = new Paragraph();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            for (int x = 0; x < title.Length; x++)
            {
                Phrase idlLine = new Phrase();
                Chunk firstItem = new Chunk(title[x], bold);
                Chunk firstContent = new Chunk(content[x]);
                idlLine.Add(firstItem);
                idlLine.Add(firstContent);
                compoundLine.Add(idlLine);
                if (x != (title.Length - 1)) { compoundLine.Add(glue); }
            }
            compoundLine.SpacingBefore = spaceBefore; //20f
            compoundLine.SpacingAfter = spaceAfter; //10.5f
            return compoundLine;
        }
        public void CreatePatientFile()
        {
            try
            {
                filePath = tempPath + "Expediente-Paciente.pdf";
                Document doc = new Document();
                PdfWriter writePDF;
                writePDF = PdfWriter.GetInstance(doc, new FileStream(filePath, FileMode.Create));
                doc.Open();
                //Titulo del documento
                Paragraph docTitle = new Paragraph("Expediente Medico");
                docTitle.Alignment = Element.ALIGN_CENTER;
                docTitle.Font.Size = 22;
                docTitle.Font = bold;
                doc.Add(docTitle);

                //Nombre de la clinica
                Paragraph docSubTitle = new Paragraph("\"Soft Hospital\"");
                docSubTitle.Alignment = Element.ALIGN_CENTER;
                docSubTitle.Font.Size = 18;
                docSubTitle.Font = bold;
                doc.Add(docSubTitle);

                //Informacion del paciente se inserta en el PDF aqui
                //Cambiar datos del ID y Fecha con valores reales
                string[] idTimeTitle = new string[2] { "ID de expediente: ", "Fecha: " };
                string[] idTimeContent = new string[2] { "1234567", DateTime.Now.Date.ToString() };
                doc.Add(SeveralItemAdd(idTimeTitle, idTimeContent, 20f, 10.5f));

                if (dialogFile != null)
                {
                    //Imagen de la base de datos
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(dialogFile);
                    jpg.ScaleAbsoluteHeight(175f);
                    jpg.ScaleAbsoluteWidth(150f);
                    jpg.Alignment = iTextSharp.text.Image.TEXTWRAP;
                    jpg.SetAbsolutePosition(400, 545);
                    doc.Add(jpg);
                }

                //Cambiar el segundo parametro para valores de la base de datos

                doc.Add(CreateLine("Nombre del paciente:", LineBreaker(emp.Nombres, 35)));
                doc.Add(CreateLine("Telefono:", "638-103-4956"));
                doc.Add(CreateLine("Fecha de nacimiento:", "17/10/1998"));
                doc.Add(CreateLine("Sexo:", "Hombre"));
                doc.Add(CreateLine("Direccion:", LineBreaker("Abelardo L. Rodriguez y Adolfo Lopez Mateos. Colonia La Deportiva. Casa Esquina", 43)));
                doc.Add(CreateLine("E-Mail:", "isacc1798@gmail.com"));
                doc.Add(CreateLine("\n", "\n"));

                //Seccion de info que ingresa el medico. 
                //Reemplazar datos en los arreglos que terminan en Info con datos de la BD.
                string[] measureTitles = new string[3] { "Peso: ", "Altura: ", "Temperatura: " };
                string[] measureInfo = new string[3] { "1.68 metros", "89 kilogramos", "31 celsius" };
                string[] healthTitles = new string[2] { "Presión Arterial: ", "Frecuencia Cardiaca: " };
                string[] healthInfo = new string[2] { "145/80", "80 por minuto" };
                doc.Add(CreateLine("Motivo de consulta:", LineBreaker("Siente dolor en el lado izquierdo de su torso", 150)));
                doc.Add(CreateLine("Antecedentes:", LineBreaker("No tiene antecedentes / Ha tenido operacion previamente por piedras en el riñon.", 150)));
                doc.Add(CreateLine("Alergias:", "Alergias a metafetamina, paracetamol"));
                doc.Add(SeveralItemAdd(measureTitles, measureInfo, 5.5f, 10.5f));
                doc.Add(SeveralItemAdd(healthTitles, healthInfo, 5.5f, 10.5f));
                doc.Add(CreateLine("Sintomas: ", "Dolor en sus rodillas"));
                doc.Add(CreateLine("Observaciones: ", "Paciente esta sobre su peso ideal para su estatura."));
                PdfContentByte page = writePDF.DirectContent;                               //Modificar por el nombre de medico
                ColumnText.ShowTextAligned(page, Element.ALIGN_LEFT, CreateLine("Doctor: ", "Funalito"), 20, 30, 0);
                ColumnText.ShowTextAligned(page, Element.ALIGN_LEFT, CreateLine("Firma: ", "____________________________"), 330, 30, 0);


                doc.Close();
                writePDF.Close();
                PrintDocument printDoc = new PrintDocument();
                printDoc.PrintPage += PrintPage;
                PrintPreviewDialog printPreview = new PrintPreviewDialog();
                printPreview.Document = printDoc;
                printPreview.ShowDialog();

            }
            catch (Exception e)
            {

                throw;
            }
        }
        public void CreatePrescriptionFile()
        {
            filePath = tempPath + "Receta-Medica.pdf";
            Document doc = new Document();
            PdfWriter writePDF;
            writePDF = PdfWriter.GetInstance(doc, new FileStream(filePath, FileMode.Create));
            doc.Open();
            //doc.SetPageSize(PageSize.A4_LANDSCAPE);
            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance("../../Resources/Receta-medica-vertical.jpg");
            jpg.ScaleAbsoluteHeight(doc.PageSize.Height);
            jpg.ScaleAbsoluteWidth(doc.PageSize.Width);
            jpg.Alignment = iTextSharp.text.Image.TEXTWRAP;
            jpg.SetAbsolutePosition(0, 0);
            doc.Add(jpg);
            //Cambiar por nombre del doctor en sesion
            Paragraph docTitle = new Paragraph("Dr. Jose Vasconcelos");
            docTitle.Alignment = Element.ALIGN_CENTER;
            docTitle.SpacingAfter = 15.5f;
            docTitle.Font.Size = 22;
            docTitle.Font = bold;
            doc.Add(docTitle);
            //Reemplazar los datos en arreglos que terminan en Info por los del BD
            string[] patientTitle = new string[2] { "Nombre: ", "Fecha: " };
            string[] patientInfo = new string[2] { Nombre_pac, DateTime.Now.Date.ToString() };
            string[] healthTitle = new string[3] { "Sexo: ", "Edad: ", "Clave: " };
            string[] healthInfo = new string[3] { Sexo_pac, Edad_pac, Clave_pac };
            //Cambiar los datos de medico por la especialidad, codigo del medico y direccion del medico
            doc.Add(CreateLineCentered("Medico " + "General"));
            doc.Add(CreateLineCentered("Cedula Profesional " + "2453453-B"));
            doc.Add(CreateLineCentered(LineBreaker("Abelardo L. Rodriguez y Adolfo Lopez Mateos. Colonia La Deportiva", 45)));
            doc.Add(CreateLine("\n", ""));
            doc.Add(SeveralItemAdd(patientTitle, patientInfo, 2.5f, 2.5f));
            doc.Add(SeveralItemAdd(healthTitle, healthInfo, 2.5f, 2.5f));
            doc.Add(CreateLine("", ""));
            doc.Add(GetMedsInfo());

            doc.Close();
            writePDF.Close();
            PrintDocument printDoc = new PrintDocument();
            //printDoc.DefaultPageSettings.Landscape = true;
            printDoc.PrintPage += PrintPage;
            PrintPreviewDialog printPreview = new PrintPreviewDialog();
            printPreview.Document = printDoc;
            printPreview.ShowDialog();
        }
        public PdfPTable GetMedsInfo()
        {
            PdfPTable table = new PdfPTable(3);
            man.Conectar();

            string queryMeds = $@"SELECT Medicamento, Dosis, Tiempodeuso FROM dbo.Receta WHERE Clave_pac LIKE '%{patientGetMeds}%';";
            using (var command = new SqlCommand(queryMeds, man.conexion))
            {
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        PdfPCell med = new PdfPCell(new Phrase(reader[0].ToString()));
                        med.Border = 0;
                        med.PaddingBottom = 5f;
                        PdfPCell dosis = new PdfPCell(new Phrase(reader[1].ToString()));
                        dosis.Border = 0;
                        dosis.PaddingBottom = 5f;
                        PdfPCell frec = new PdfPCell(new Phrase(reader[2].ToString()));
                        frec.Border = 0;
                        frec.PaddingBottom = 5f;

                        table.AddCell(med);
                        table.AddCell(dosis);
                        table.AddCell(frec);
                    }
                }
            }
            return table;
        }
        public void PrintPage(object sender, PrintPageEventArgs e)
        {
            PDFToImage convertPDF = new PDFToImage();
            try
            {
                using (var memoryStream = new MemoryStream(File.ReadAllBytes(convertPDF.ConvertPDFVertical(tempPath, filePath))))
                {
                    using (var image = System.Drawing.Image.FromStream(memoryStream))
                    {
                        e.Graphics.DrawImage(image, e.PageBounds);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "  " + filePath);
            }
        }
        public void Table_Load(string searchQuery, DataGridView table, List<String> ColumnNames)
        {
            DataTable Table = new DataTable();
            SqlDataAdapter data = new SqlDataAdapter();
            string selectGuests = searchQuery;
            data.SelectCommand = new SqlCommand(selectGuests, man.conexion);
            data.Fill(Table);
            Column_Name(Table, ColumnNames);
            BindingSource bind = new BindingSource();
            bind.DataSource = Table;
            table.DataSource = bind;
        }
        private void Column_Name(DataTable Table, List<String> ColumnNameList)
        {
            for (int i = 0; i < Table.Columns.Count; i++)
            {
                Table.Columns[i].ColumnName = ColumnNameList[i];
            }
        }
    }
    public class PDFToImage
    {
        public string ConvertPDFVertical(string path, string pdf)
        {
            var settings = new MagickReadSettings();
            settings.Density = new Density(300);

            using (var images = new MagickImageCollection())
            {
                // Add all the pages of the pdf file to the collection
                images.Read(pdf, settings);
                // Create new image that appends all the pages vertically
                using (var vertical = images.AppendVertically())
                {
                    // Save result as a png
                    vertical.Write(path + "image.jpg");
                }
            }
            return (path + "image.jpg");
        }

        public string ConvertPDFHorizontal(string path, string pdf)
        {
            var settings = new MagickReadSettings();
            settings.Density = new Density(300);

            using (var images = new MagickImageCollection())
            {
                // Add all the pages of the pdf file to the collection
                images.Read(pdf, settings);

                // Create new image that appends all the pages horizontally
                using (var horizontal = images.AppendHorizontally())
                {
                    // Save result as a png
                    horizontal.Write("Snakeware.horizontal.png");
                }
            }
            return (path + "image.jpg");
        }
    }
    public class MedsInfo
    {
        public string medicamento { get; set; }
        public string dosis { get; set; }
        public string frec { get; set; }
    }
}