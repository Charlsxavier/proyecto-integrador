﻿using System;

namespace La_Estrellita
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Lbl_SoftHospital = new System.Windows.Forms.Label();
            this.navigationPage8 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.metroButton18 = new MetroFramework.Controls.MetroButton();
            this.metroButton13 = new MetroFramework.Controls.MetroButton();
            this.metroButton14 = new MetroFramework.Controls.MetroButton();
            this.metroButton15 = new MetroFramework.Controls.MetroButton();
            this.metroButton16 = new MetroFramework.Controls.MetroButton();
            this.metroButton17 = new MetroFramework.Controls.MetroButton();
            this.label29 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.exit_recepcionista = new System.Windows.Forms.PictureBox();
            this.exit_admin = new System.Windows.Forms.PictureBox();
            this.Btn_Exit = new System.Windows.Forms.PictureBox();
            this.NFrame_SoftHospital = new DevExpress.XtraBars.Navigation.NavigationFrame();
            this.Page_Login = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Tbox_Usuario = new MetroFramework.Controls.MetroTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Tbox_Contraseña = new MetroFramework.Controls.MetroTextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.Btn_Acceder = new MetroFramework.Controls.MetroButton();
            this.Page_Doctor = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.label2 = new System.Windows.Forms.Label();
            this.Btn_Receta = new MetroFramework.Controls.MetroButton();
            this.Btn_Expediente = new MetroFramework.Controls.MetroButton();
            this.Page_Recep = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.label3 = new System.Windows.Forms.Label();
            this.Btn_Agenda = new MetroFramework.Controls.MetroButton();
            this.Btn_RegistroP = new MetroFramework.Controls.MetroButton();
            this.Page_Admin = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.label6 = new System.Windows.Forms.Label();
            this.Btn_RegistroR = new MetroFramework.Controls.MetroButton();
            this.Btn_RegistroD = new MetroFramework.Controls.MetroButton();
            this.Page_Expedientes = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.imprimir_expediente_btn = new MetroFramework.Controls.MetroButton();
            this.Btn_CargarE = new MetroFramework.Controls.MetroButton();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.Tbox_Observaciones = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.Tbox_Sintomas = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.Tbox_Motivo = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.Tbox_Alergias = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.Tbox_Frec = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.Tbox_Presion = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.Tbox_Temp = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.Tbox_Altura = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.Tbox_Peso = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.Tbox_Antec = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.return_expediente_btn = new MetroFramework.Controls.MetroButton();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.navigationPage4 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.DGV_Receta = new System.Windows.Forms.DataGridView();
            this.eliminar_receta_btn = new MetroFramework.Controls.MetroButton();
            this.imprimir_receta_btn = new MetroFramework.Controls.MetroButton();
            this.return_receta_btn = new MetroFramework.Controls.MetroButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Tbox_FrecUso = new System.Windows.Forms.TextBox();
            this.Tbox_TieUso = new System.Windows.Forms.TextBox();
            this.Tbox_Dosis = new System.Windows.Forms.TextBox();
            this.Tbox_Medicamento = new System.Windows.Forms.TextBox();
            this.Btn_AgregarReceta = new MetroFramework.Controls.MetroButton();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Page_RegistroP = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.fechaConsulta = new System.Windows.Forms.DateTimePicker();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.Pbox_FotoP = new System.Windows.Forms.PictureBox();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbox_edadP = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.Tbox_Clavepac = new System.Windows.Forms.TextBox();
            this.Tbox_NombreP = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Cbox_Doctor = new System.Windows.Forms.ComboBox();
            this.Tbox_TelefonoP = new System.Windows.Forms.MaskedTextBox();
            this.Cbox_Sexo = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.Tbox_MailP = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.Tbox_DireccionP = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Tbox_ApellidosP = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.Page_Agenda = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.Page_RegistroD = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.Btn_GuardarD = new MetroFramework.Controls.MetroButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Btn_CargarFotoD = new MetroFramework.Controls.MetroButton();
            this.Pbox_FotoD = new System.Windows.Forms.PictureBox();
            this.label36 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Tbox_ClaveD = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.Tbox_ContraD = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.Tbox_UsuarioD = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.Tbox_EspecialidadD = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.Tbox_MailD = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.Tbox_DireccionD = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.Tbox_TelefonoD = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.Tbox_NombreD = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.Page_RegistroR = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.Btn_GuardarR = new MetroFramework.Controls.MetroButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.Btn_CargarFotoR = new MetroFramework.Controls.MetroButton();
            this.Pbox_FotoR = new System.Windows.Forms.PictureBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.Tbox_UsuarioR = new System.Windows.Forms.TextBox();
            this.Tbox_ContraR = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.Tbox_ClaveR = new System.Windows.Forms.TextBox();
            this.Tbox_MailR = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.Tbox_DireccionR = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.Tbox_TelefonoR = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.Tbox_NombreR = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.navigationPage1 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.label44 = new System.Windows.Forms.Label();
            this.selectPatient_btn = new MetroFramework.Controls.MetroButton();
            this.return_patient_search_btn = new MetroFramework.Controls.MetroButton();
            this.label1 = new System.Windows.Forms.Label();
            this.search_patient_txtbox = new System.Windows.Forms.TextBox();
            this.patient_table = new System.Windows.Forms.DataGridView();
            this.metroContextMenu1 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.printPreview = new System.Windows.Forms.PrintPreviewDialog();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.tbox_usudoc = new System.Windows.Forms.TextBox();
            this.tbox_condoc = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.tbox_clave = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.tbox_clavep = new System.Windows.Forms.TextBox();
            this.DGV_Expedientes = new System.Windows.Forms.DataGridView();
            this.navigationPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exit_recepcionista)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exit_admin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NFrame_SoftHospital)).BeginInit();
            this.NFrame_SoftHospital.SuspendLayout();
            this.Page_Login.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.Page_Doctor.SuspendLayout();
            this.Page_Recep.SuspendLayout();
            this.Page_Admin.SuspendLayout();
            this.Page_Expedientes.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.navigationPage4.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Receta)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.Page_RegistroP.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Pbox_FotoP)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.Page_Agenda.SuspendLayout();
            this.Page_RegistroD.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Pbox_FotoD)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.Page_RegistroR.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Pbox_FotoR)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.navigationPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patient_table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Expedientes)).BeginInit();
            this.SuspendLayout();
            // 
            // Lbl_SoftHospital
            // 
            this.Lbl_SoftHospital.AutoSize = true;
            this.Lbl_SoftHospital.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_SoftHospital.ForeColor = System.Drawing.Color.White;
            this.Lbl_SoftHospital.Location = new System.Drawing.Point(332, 12);
            this.Lbl_SoftHospital.Name = "Lbl_SoftHospital";
            this.Lbl_SoftHospital.Size = new System.Drawing.Size(290, 32);
            this.Lbl_SoftHospital.TabIndex = 1;
            this.Lbl_SoftHospital.Text = "\"SOFT HOSPITAL\"";
            this.Lbl_SoftHospital.Click += new System.EventHandler(this.label1_Click);
            // 
            // navigationPage8
            // 
            this.navigationPage8.Caption = "navigationPage8";
            this.navigationPage8.Controls.Add(this.metroButton18);
            this.navigationPage8.Controls.Add(this.metroButton13);
            this.navigationPage8.Controls.Add(this.metroButton14);
            this.navigationPage8.Controls.Add(this.metroButton15);
            this.navigationPage8.Controls.Add(this.metroButton16);
            this.navigationPage8.Controls.Add(this.metroButton17);
            this.navigationPage8.Controls.Add(this.label29);
            this.navigationPage8.Name = "navigationPage8";
            this.navigationPage8.Size = new System.Drawing.Size(938, 459);
            // 
            // metroButton18
            // 
            this.metroButton18.BackColor = System.Drawing.Color.Transparent;
            this.metroButton18.Location = new System.Drawing.Point(54, 171);
            this.metroButton18.Name = "metroButton18";
            this.metroButton18.Size = new System.Drawing.Size(70, 68);
            this.metroButton18.TabIndex = 29;
            this.metroButton18.Text = "metroButton18";
            this.metroButton18.UseSelectable = true;
            // 
            // metroButton13
            // 
            this.metroButton13.Location = new System.Drawing.Point(307, 335);
            this.metroButton13.Name = "metroButton13";
            this.metroButton13.Size = new System.Drawing.Size(292, 40);
            this.metroButton13.TabIndex = 28;
            this.metroButton13.Text = "Obtener kilometros recorridos por unidad";
            this.metroButton13.UseSelectable = true;
            // 
            // metroButton14
            // 
            this.metroButton14.Location = new System.Drawing.Point(307, 277);
            this.metroButton14.Name = "metroButton14";
            this.metroButton14.Size = new System.Drawing.Size(292, 40);
            this.metroButton14.TabIndex = 27;
            this.metroButton14.Text = "Filtro por nombre de unidad";
            this.metroButton14.UseSelectable = true;
            // 
            // metroButton15
            // 
            this.metroButton15.Location = new System.Drawing.Point(307, 217);
            this.metroButton15.Name = "metroButton15";
            this.metroButton15.Size = new System.Drawing.Size(292, 40);
            this.metroButton15.TabIndex = 26;
            this.metroButton15.Text = "Unidades que consumen menos de 50 litros";
            this.metroButton15.UseSelectable = true;
            // 
            // metroButton16
            // 
            this.metroButton16.Location = new System.Drawing.Point(307, 155);
            this.metroButton16.Name = "metroButton16";
            this.metroButton16.Size = new System.Drawing.Size(292, 40);
            this.metroButton16.TabIndex = 25;
            this.metroButton16.Text = "Unidades que consumen mas de 100 litros";
            this.metroButton16.UseSelectable = true;
            // 
            // metroButton17
            // 
            this.metroButton17.Location = new System.Drawing.Point(307, 96);
            this.metroButton17.Name = "metroButton17";
            this.metroButton17.Size = new System.Drawing.Size(292, 40);
            this.metroButton17.TabIndex = 24;
            this.metroButton17.Text = "Todos los reportes";
            this.metroButton17.UseSelectable = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label29.Location = new System.Drawing.Point(5, 10);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(94, 23);
            this.label29.TabIndex = 23;
            this.label29.Text = "Reportes";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Image = global::La_Estrellita.Properties.Resources.logout;
            this.pictureBox4.Location = new System.Drawing.Point(3, 539);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(60, 54);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 62;
            this.pictureBox4.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox4, "Click para cerrar sesion");
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // exit_recepcionista
            // 
            this.exit_recepcionista.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit_recepcionista.Image = global::La_Estrellita.Properties.Resources.logout;
            this.exit_recepcionista.Location = new System.Drawing.Point(3, 538);
            this.exit_recepcionista.Name = "exit_recepcionista";
            this.exit_recepcionista.Size = new System.Drawing.Size(59, 54);
            this.exit_recepcionista.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.exit_recepcionista.TabIndex = 85;
            this.exit_recepcionista.TabStop = false;
            this.toolTip1.SetToolTip(this.exit_recepcionista, "Click para cerrar sesion");
            this.exit_recepcionista.Click += new System.EventHandler(this.Exit_recepcionista_Click);
            // 
            // exit_admin
            // 
            this.exit_admin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit_admin.Image = global::La_Estrellita.Properties.Resources.logout;
            this.exit_admin.Location = new System.Drawing.Point(3, 538);
            this.exit_admin.Name = "exit_admin";
            this.exit_admin.Size = new System.Drawing.Size(57, 54);
            this.exit_admin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.exit_admin.TabIndex = 83;
            this.exit_admin.TabStop = false;
            this.toolTip1.SetToolTip(this.exit_admin, "Click para cerrar sesion");
            this.exit_admin.Click += new System.EventHandler(this.Exit_admin_Click);
            // 
            // Btn_Exit
            // 
            this.Btn_Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Exit.Image = global::La_Estrellita.Properties.Resources.icons8_multiply_52;
            this.Btn_Exit.Location = new System.Drawing.Point(930, 12);
            this.Btn_Exit.Name = "Btn_Exit";
            this.Btn_Exit.Size = new System.Drawing.Size(20, 19);
            this.Btn_Exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Btn_Exit.TabIndex = 2;
            this.Btn_Exit.TabStop = false;
            this.Btn_Exit.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // NFrame_SoftHospital
            // 
            this.NFrame_SoftHospital.Appearance.BackColor = System.Drawing.Color.White;
            this.NFrame_SoftHospital.Appearance.Options.UseBackColor = true;
            this.NFrame_SoftHospital.BackgroundImage = global::La_Estrellita.Properties.Resources.fondo;
            this.NFrame_SoftHospital.Controls.Add(this.Page_Login);
            this.NFrame_SoftHospital.Controls.Add(this.Page_Doctor);
            this.NFrame_SoftHospital.Controls.Add(this.Page_Recep);
            this.NFrame_SoftHospital.Controls.Add(this.Page_Admin);
            this.NFrame_SoftHospital.Controls.Add(this.Page_Expedientes);
            this.NFrame_SoftHospital.Controls.Add(this.navigationPage4);
            this.NFrame_SoftHospital.Controls.Add(this.Page_RegistroP);
            this.NFrame_SoftHospital.Controls.Add(this.Page_Agenda);
            this.NFrame_SoftHospital.Controls.Add(this.Page_RegistroD);
            this.NFrame_SoftHospital.Controls.Add(this.Page_RegistroR);
            this.NFrame_SoftHospital.Controls.Add(this.navigationPage1);
            this.NFrame_SoftHospital.Location = new System.Drawing.Point(12, 53);
            this.NFrame_SoftHospital.Name = "NFrame_SoftHospital";
            this.NFrame_SoftHospital.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.Page_Login,
            this.Page_Doctor,
            this.Page_Recep,
            this.Page_Admin,
            this.Page_Expedientes,
            this.navigationPage4,
            this.Page_RegistroP,
            this.Page_Agenda,
            this.Page_RegistroD,
            this.Page_RegistroR,
            this.navigationPage1});
            this.NFrame_SoftHospital.SelectedPage = this.Page_Doctor;
            this.NFrame_SoftHospital.Size = new System.Drawing.Size(938, 596);
            this.NFrame_SoftHospital.TabIndex = 0;
            this.NFrame_SoftHospital.Text = "navigationFrame1";
            this.NFrame_SoftHospital.TransitionType = DevExpress.Utils.Animation.Transitions.Push;
            this.NFrame_SoftHospital.SelectedPageChanged += new DevExpress.XtraBars.Navigation.SelectedPageChangedEventHandler(this.NFrame_SoftHospital_SelectedPageChanged);
            // 
            // Page_Login
            // 
            this.Page_Login.AutoSize = true;
            this.Page_Login.Controls.Add(this.groupBox1);
            this.Page_Login.Controls.Add(this.pictureBox3);
            this.Page_Login.Controls.Add(this.Btn_Acceder);
            this.Page_Login.Name = "Page_Login";
            this.Page_Login.Size = new System.Drawing.Size(938, 596);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Tbox_Usuario);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Tbox_Contraseña);
            this.groupBox1.Location = new System.Drawing.Point(233, 318);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(450, 150);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            // 
            // Tbox_Usuario
            // 
            // 
            // 
            // 
            this.Tbox_Usuario.CustomButton.Image = null;
            this.Tbox_Usuario.CustomButton.Location = new System.Drawing.Point(99, 1);
            this.Tbox_Usuario.CustomButton.Name = "";
            this.Tbox_Usuario.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Tbox_Usuario.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Tbox_Usuario.CustomButton.TabIndex = 1;
            this.Tbox_Usuario.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Tbox_Usuario.CustomButton.UseSelectable = true;
            this.Tbox_Usuario.CustomButton.Visible = false;
            this.Tbox_Usuario.Lines = new string[0];
            this.Tbox_Usuario.Location = new System.Drawing.Point(245, 40);
            this.Tbox_Usuario.MaxLength = 32767;
            this.Tbox_Usuario.Name = "Tbox_Usuario";
            this.Tbox_Usuario.PasswordChar = '\0';
            this.Tbox_Usuario.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Tbox_Usuario.SelectedText = "";
            this.Tbox_Usuario.SelectionLength = 0;
            this.Tbox_Usuario.SelectionStart = 0;
            this.Tbox_Usuario.ShortcutsEnabled = true;
            this.Tbox_Usuario.Size = new System.Drawing.Size(121, 23);
            this.Tbox_Usuario.TabIndex = 13;
            this.Tbox_Usuario.UseSelectable = true;
            this.Tbox_Usuario.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Tbox_Usuario.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(58, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 23);
            this.label4.TabIndex = 11;
            this.label4.Text = "Usuario:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.label5.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(58, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 23);
            this.label5.TabIndex = 12;
            this.label5.Text = "Contraseña:";
            // 
            // Tbox_Contraseña
            // 
            // 
            // 
            // 
            this.Tbox_Contraseña.CustomButton.Image = null;
            this.Tbox_Contraseña.CustomButton.Location = new System.Drawing.Point(95, 1);
            this.Tbox_Contraseña.CustomButton.Name = "";
            this.Tbox_Contraseña.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Tbox_Contraseña.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Tbox_Contraseña.CustomButton.TabIndex = 1;
            this.Tbox_Contraseña.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Tbox_Contraseña.CustomButton.UseSelectable = true;
            this.Tbox_Contraseña.CustomButton.Visible = false;
            this.Tbox_Contraseña.Lines = new string[0];
            this.Tbox_Contraseña.Location = new System.Drawing.Point(245, 102);
            this.Tbox_Contraseña.MaxLength = 32767;
            this.Tbox_Contraseña.Name = "Tbox_Contraseña";
            this.Tbox_Contraseña.PasswordChar = '●';
            this.Tbox_Contraseña.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Tbox_Contraseña.SelectedText = "";
            this.Tbox_Contraseña.SelectionLength = 0;
            this.Tbox_Contraseña.SelectionStart = 0;
            this.Tbox_Contraseña.ShortcutsEnabled = true;
            this.Tbox_Contraseña.Size = new System.Drawing.Size(117, 23);
            this.Tbox_Contraseña.TabIndex = 14;
            this.Tbox_Contraseña.UseSelectable = true;
            this.Tbox_Contraseña.UseSystemPasswordChar = true;
            this.Tbox_Contraseña.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Tbox_Contraseña.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(400, 83);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(191, 193);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 20;
            this.pictureBox3.TabStop = false;
            // 
            // Btn_Acceder
            // 
            this.Btn_Acceder.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Acceder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Acceder.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.Btn_Acceder.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.Btn_Acceder.Location = new System.Drawing.Point(400, 523);
            this.Btn_Acceder.Name = "Btn_Acceder";
            this.Btn_Acceder.Size = new System.Drawing.Size(142, 37);
            this.Btn_Acceder.TabIndex = 15;
            this.Btn_Acceder.Text = "Acceder";
            this.Btn_Acceder.UseSelectable = true;
            this.Btn_Acceder.Click += new System.EventHandler(this.Btn_Acceder_Click);
            // 
            // Page_Doctor
            // 
            this.Page_Doctor.AutoSize = true;
            this.Page_Doctor.Controls.Add(this.label2);
            this.Page_Doctor.Controls.Add(this.pictureBox4);
            this.Page_Doctor.Controls.Add(this.Btn_Receta);
            this.Page_Doctor.Controls.Add(this.Btn_Expediente);
            this.Page_Doctor.Name = "Page_Doctor";
            this.Page_Doctor.Size = new System.Drawing.Size(938, 596);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 23);
            this.label2.TabIndex = 81;
            this.label2.Text = "Doctor:";
            // 
            // Btn_Receta
            // 
            this.Btn_Receta.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Receta.BackgroundImage")));
            this.Btn_Receta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Btn_Receta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Receta.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.Btn_Receta.Location = new System.Drawing.Point(557, 223);
            this.Btn_Receta.Name = "Btn_Receta";
            this.Btn_Receta.Size = new System.Drawing.Size(199, 125);
            this.Btn_Receta.TabIndex = 61;
            this.Btn_Receta.Text = "RECETA MEDICA";
            this.Btn_Receta.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Btn_Receta.UseSelectable = true;
            this.Btn_Receta.Click += new System.EventHandler(this.metroButton31_Click_1);
            // 
            // Btn_Expediente
            // 
            this.Btn_Expediente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Expediente.BackgroundImage")));
            this.Btn_Expediente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Btn_Expediente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Expediente.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.Btn_Expediente.ForeColor = System.Drawing.Color.Black;
            this.Btn_Expediente.Location = new System.Drawing.Point(193, 223);
            this.Btn_Expediente.Name = "Btn_Expediente";
            this.Btn_Expediente.Size = new System.Drawing.Size(199, 125);
            this.Btn_Expediente.TabIndex = 59;
            this.Btn_Expediente.Text = "EXPEDIENTES";
            this.Btn_Expediente.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Btn_Expediente.UseSelectable = true;
            this.Btn_Expediente.Click += new System.EventHandler(this.metroButton23_Click_1);
            this.Btn_Expediente.MouseUp += new System.Windows.Forms.MouseEventHandler(this.metroButton23_MouseDown);
            // 
            // Page_Recep
            // 
            this.Page_Recep.AutoSize = true;
            this.Page_Recep.Controls.Add(this.exit_recepcionista);
            this.Page_Recep.Controls.Add(this.label3);
            this.Page_Recep.Controls.Add(this.Btn_Agenda);
            this.Page_Recep.Controls.Add(this.Btn_RegistroP);
            this.Page_Recep.Name = "Page_Recep";
            this.Page_Recep.Size = new System.Drawing.Size(938, 596);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 23);
            this.label3.TabIndex = 84;
            this.label3.Text = "Recepcionista:";
            // 
            // Btn_Agenda
            // 
            this.Btn_Agenda.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_Agenda.BackgroundImage")));
            this.Btn_Agenda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Btn_Agenda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Agenda.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.Btn_Agenda.Location = new System.Drawing.Point(557, 223);
            this.Btn_Agenda.Name = "Btn_Agenda";
            this.Btn_Agenda.Size = new System.Drawing.Size(199, 147);
            this.Btn_Agenda.TabIndex = 83;
            this.Btn_Agenda.Text = "AGENDA";
            this.Btn_Agenda.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Btn_Agenda.UseSelectable = true;
            this.Btn_Agenda.Click += new System.EventHandler(this.MetroButton1_Click_2);
            // 
            // Btn_RegistroP
            // 
            this.Btn_RegistroP.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_RegistroP.BackgroundImage")));
            this.Btn_RegistroP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Btn_RegistroP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_RegistroP.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.Btn_RegistroP.ForeColor = System.Drawing.Color.Black;
            this.Btn_RegistroP.Location = new System.Drawing.Point(193, 223);
            this.Btn_RegistroP.Name = "Btn_RegistroP";
            this.Btn_RegistroP.Size = new System.Drawing.Size(199, 147);
            this.Btn_RegistroP.TabIndex = 81;
            this.Btn_RegistroP.Text = "REGISTRO DE PACIENTES";
            this.Btn_RegistroP.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Btn_RegistroP.UseSelectable = true;
            this.Btn_RegistroP.Click += new System.EventHandler(this.MetroButton3_Click_2);
            // 
            // Page_Admin
            // 
            this.Page_Admin.AutoSize = true;
            this.Page_Admin.Controls.Add(this.exit_admin);
            this.Page_Admin.Controls.Add(this.label6);
            this.Page_Admin.Controls.Add(this.Btn_RegistroR);
            this.Page_Admin.Controls.Add(this.Btn_RegistroD);
            this.Page_Admin.Name = "Page_Admin";
            this.Page_Admin.Size = new System.Drawing.Size(938, 596);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(171, 23);
            this.label6.TabIndex = 82;
            this.label6.Text = "Administrador:";
            // 
            // Btn_RegistroR
            // 
            this.Btn_RegistroR.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_RegistroR.BackgroundImage")));
            this.Btn_RegistroR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Btn_RegistroR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_RegistroR.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.Btn_RegistroR.Location = new System.Drawing.Point(557, 223);
            this.Btn_RegistroR.Name = "Btn_RegistroR";
            this.Btn_RegistroR.Size = new System.Drawing.Size(199, 147);
            this.Btn_RegistroR.TabIndex = 65;
            this.Btn_RegistroR.Text = "REGISTRO DE RECEPCIONISTAS";
            this.Btn_RegistroR.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Btn_RegistroR.UseSelectable = true;
            this.Btn_RegistroR.Click += new System.EventHandler(this.Btn_RegistroR_Click_2);
            // 
            // Btn_RegistroD
            // 
            this.Btn_RegistroD.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btn_RegistroD.BackgroundImage")));
            this.Btn_RegistroD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Btn_RegistroD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_RegistroD.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.Btn_RegistroD.ForeColor = System.Drawing.Color.Black;
            this.Btn_RegistroD.Location = new System.Drawing.Point(193, 223);
            this.Btn_RegistroD.Name = "Btn_RegistroD";
            this.Btn_RegistroD.Size = new System.Drawing.Size(199, 147);
            this.Btn_RegistroD.TabIndex = 63;
            this.Btn_RegistroD.Text = "REGISTRO DE DOCTORES";
            this.Btn_RegistroD.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Btn_RegistroD.UseSelectable = true;
            this.Btn_RegistroD.Click += new System.EventHandler(this.metroButton7_Click_2);
            // 
            // Page_Expedientes
            // 
            this.Page_Expedientes.AutoSize = true;
            this.Page_Expedientes.Controls.Add(this.imprimir_expediente_btn);
            this.Page_Expedientes.Controls.Add(this.Btn_CargarE);
            this.Page_Expedientes.Controls.Add(this.groupBox11);
            this.Page_Expedientes.Controls.Add(this.return_expediente_btn);
            this.Page_Expedientes.Controls.Add(this.DGV_Expedientes);
            this.Page_Expedientes.Controls.Add(this.label8);
            this.Page_Expedientes.Controls.Add(this.label7);
            this.Page_Expedientes.Name = "Page_Expedientes";
            this.Page_Expedientes.Size = new System.Drawing.Size(941, 596);
            // 
            // imprimir_expediente_btn
            // 
            this.imprimir_expediente_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imprimir_expediente_btn.Location = new System.Drawing.Point(669, 410);
            this.imprimir_expediente_btn.Name = "imprimir_expediente_btn";
            this.imprimir_expediente_btn.Size = new System.Drawing.Size(98, 28);
            this.imprimir_expediente_btn.TabIndex = 123;
            this.imprimir_expediente_btn.Text = "Imprimir";
            this.imprimir_expediente_btn.UseSelectable = true;
            this.imprimir_expediente_btn.Click += new System.EventHandler(this.Imprimir_expediente_btn_Click);
            // 
            // Btn_CargarE
            // 
            this.Btn_CargarE.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_CargarE.Location = new System.Drawing.Point(229, 410);
            this.Btn_CargarE.Name = "Btn_CargarE";
            this.Btn_CargarE.Size = new System.Drawing.Size(98, 28);
            this.Btn_CargarE.TabIndex = 122;
            this.Btn_CargarE.Text = "Cargar";
            this.Btn_CargarE.UseSelectable = true;
            this.Btn_CargarE.Click += new System.EventHandler(this.MetroButton32_Click_1);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.Tbox_Observaciones);
            this.groupBox11.Controls.Add(this.label61);
            this.groupBox11.Controls.Add(this.Tbox_Sintomas);
            this.groupBox11.Controls.Add(this.label60);
            this.groupBox11.Controls.Add(this.Tbox_Motivo);
            this.groupBox11.Controls.Add(this.label59);
            this.groupBox11.Controls.Add(this.Tbox_Alergias);
            this.groupBox11.Controls.Add(this.label58);
            this.groupBox11.Controls.Add(this.Tbox_Frec);
            this.groupBox11.Controls.Add(this.label57);
            this.groupBox11.Controls.Add(this.Tbox_Presion);
            this.groupBox11.Controls.Add(this.label56);
            this.groupBox11.Controls.Add(this.Tbox_Temp);
            this.groupBox11.Controls.Add(this.label55);
            this.groupBox11.Controls.Add(this.Tbox_Altura);
            this.groupBox11.Controls.Add(this.label54);
            this.groupBox11.Controls.Add(this.Tbox_Peso);
            this.groupBox11.Controls.Add(this.label53);
            this.groupBox11.Controls.Add(this.Tbox_Antec);
            this.groupBox11.Controls.Add(this.label52);
            this.groupBox11.Location = new System.Drawing.Point(19, 78);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(495, 308);
            this.groupBox11.TabIndex = 121;
            this.groupBox11.TabStop = false;
            // 
            // Tbox_Observaciones
            // 
            this.Tbox_Observaciones.Location = new System.Drawing.Point(171, 266);
            this.Tbox_Observaciones.Name = "Tbox_Observaciones";
            this.Tbox_Observaciones.Size = new System.Drawing.Size(252, 20);
            this.Tbox_Observaciones.TabIndex = 137;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(6, 266);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(132, 20);
            this.label61.TabIndex = 136;
            this.label61.Text = "Observaciones:";
            // 
            // Tbox_Sintomas
            // 
            this.Tbox_Sintomas.Location = new System.Drawing.Point(171, 235);
            this.Tbox_Sintomas.Name = "Tbox_Sintomas";
            this.Tbox_Sintomas.Size = new System.Drawing.Size(252, 20);
            this.Tbox_Sintomas.TabIndex = 135;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(6, 235);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(89, 20);
            this.label60.TabIndex = 134;
            this.label60.Text = "Sintomas:";
            // 
            // Tbox_Motivo
            // 
            this.Tbox_Motivo.Location = new System.Drawing.Point(171, 205);
            this.Tbox_Motivo.Name = "Tbox_Motivo";
            this.Tbox_Motivo.Size = new System.Drawing.Size(252, 20);
            this.Tbox_Motivo.TabIndex = 133;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(6, 205);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(142, 20);
            this.label59.TabIndex = 132;
            this.label59.Text = "Motivo Consulta:";
            // 
            // Tbox_Alergias
            // 
            this.Tbox_Alergias.Location = new System.Drawing.Point(171, 175);
            this.Tbox_Alergias.Name = "Tbox_Alergias";
            this.Tbox_Alergias.Size = new System.Drawing.Size(252, 20);
            this.Tbox_Alergias.TabIndex = 131;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(6, 175);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(79, 20);
            this.label58.TabIndex = 130;
            this.label58.Text = "Alergias:";
            // 
            // Tbox_Frec
            // 
            this.Tbox_Frec.Location = new System.Drawing.Point(171, 146);
            this.Tbox_Frec.Name = "Tbox_Frec";
            this.Tbox_Frec.Size = new System.Drawing.Size(252, 20);
            this.Tbox_Frec.TabIndex = 129;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(6, 146);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(128, 20);
            this.label57.TabIndex = 128;
            this.label57.Text = "Frec. cardiaca:";
            // 
            // Tbox_Presion
            // 
            this.Tbox_Presion.Location = new System.Drawing.Point(171, 120);
            this.Tbox_Presion.Name = "Tbox_Presion";
            this.Tbox_Presion.Size = new System.Drawing.Size(252, 20);
            this.Tbox_Presion.TabIndex = 127;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(6, 120);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(135, 20);
            this.label56.TabIndex = 126;
            this.label56.Text = "Presion arterial:";
            // 
            // Tbox_Temp
            // 
            this.Tbox_Temp.Location = new System.Drawing.Point(171, 94);
            this.Tbox_Temp.Name = "Tbox_Temp";
            this.Tbox_Temp.Size = new System.Drawing.Size(252, 20);
            this.Tbox_Temp.TabIndex = 125;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(6, 94);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(116, 20);
            this.label55.TabIndex = 124;
            this.label55.Text = "Temperatura:";
            // 
            // Tbox_Altura
            // 
            this.Tbox_Altura.Location = new System.Drawing.Point(171, 68);
            this.Tbox_Altura.Name = "Tbox_Altura";
            this.Tbox_Altura.Size = new System.Drawing.Size(252, 20);
            this.Tbox_Altura.TabIndex = 123;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(6, 68);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(62, 20);
            this.label54.TabIndex = 122;
            this.label54.Text = "Altura:";
            // 
            // Tbox_Peso
            // 
            this.Tbox_Peso.Location = new System.Drawing.Point(171, 42);
            this.Tbox_Peso.Name = "Tbox_Peso";
            this.Tbox_Peso.Size = new System.Drawing.Size(252, 20);
            this.Tbox_Peso.TabIndex = 121;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(6, 42);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(54, 20);
            this.label53.TabIndex = 120;
            this.label53.Text = "Peso:";
            // 
            // Tbox_Antec
            // 
            this.Tbox_Antec.Location = new System.Drawing.Point(171, 16);
            this.Tbox_Antec.Name = "Tbox_Antec";
            this.Tbox_Antec.Size = new System.Drawing.Size(252, 20);
            this.Tbox_Antec.TabIndex = 119;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(6, 16);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(126, 20);
            this.label52.TabIndex = 118;
            this.label52.Text = "Antecedentes:";
            // 
            // return_expediente_btn
            // 
            this.return_expediente_btn.BackColor = System.Drawing.Color.White;
            this.return_expediente_btn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("return_expediente_btn.BackgroundImage")));
            this.return_expediente_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.return_expediente_btn.Location = new System.Drawing.Point(3, 542);
            this.return_expediente_btn.Name = "return_expediente_btn";
            this.return_expediente_btn.Size = new System.Drawing.Size(50, 50);
            this.return_expediente_btn.TabIndex = 120;
            this.return_expediente_btn.UseSelectable = true;
            this.return_expediente_btn.Click += new System.EventHandler(this.Return_expediente_btn_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(397, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(160, 23);
            this.label8.TabIndex = 83;
            this.label8.Text = "EXPEDIENTES";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 23);
            this.label7.TabIndex = 82;
            this.label7.Text = "Doctor:";
            // 
            // navigationPage4
            // 
            this.navigationPage4.AutoSize = true;
            this.navigationPage4.Controls.Add(this.groupBox9);
            this.navigationPage4.Controls.Add(this.return_receta_btn);
            this.navigationPage4.Controls.Add(this.groupBox2);
            this.navigationPage4.Controls.Add(this.label10);
            this.navigationPage4.Controls.Add(this.label11);
            this.navigationPage4.Name = "navigationPage4";
            this.navigationPage4.Size = new System.Drawing.Size(938, 598);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.DGV_Receta);
            this.groupBox9.Controls.Add(this.eliminar_receta_btn);
            this.groupBox9.Controls.Add(this.imprimir_receta_btn);
            this.groupBox9.Location = new System.Drawing.Point(456, 61);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox9.Size = new System.Drawing.Size(468, 460);
            this.groupBox9.TabIndex = 119;
            this.groupBox9.TabStop = false;
            // 
            // DGV_Receta
            // 
            this.DGV_Receta.AllowUserToAddRows = false;
            this.DGV_Receta.AllowUserToDeleteRows = false;
            this.DGV_Receta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Receta.Location = new System.Drawing.Point(13, 63);
            this.DGV_Receta.Name = "DGV_Receta";
            this.DGV_Receta.RowHeadersWidth = 51;
            this.DGV_Receta.Size = new System.Drawing.Size(442, 265);
            this.DGV_Receta.TabIndex = 115;
            // 
            // eliminar_receta_btn
            // 
            this.eliminar_receta_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.eliminar_receta_btn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.eliminar_receta_btn.Location = new System.Drawing.Point(313, 407);
            this.eliminar_receta_btn.Name = "eliminar_receta_btn";
            this.eliminar_receta_btn.Size = new System.Drawing.Size(142, 37);
            this.eliminar_receta_btn.TabIndex = 119;
            this.eliminar_receta_btn.Text = "Eliminar";
            this.eliminar_receta_btn.UseSelectable = true;
            // 
            // imprimir_receta_btn
            // 
            this.imprimir_receta_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imprimir_receta_btn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.imprimir_receta_btn.Location = new System.Drawing.Point(13, 407);
            this.imprimir_receta_btn.Name = "imprimir_receta_btn";
            this.imprimir_receta_btn.Size = new System.Drawing.Size(142, 37);
            this.imprimir_receta_btn.TabIndex = 119;
            this.imprimir_receta_btn.Text = "Imprimir";
            this.imprimir_receta_btn.UseSelectable = true;
            this.imprimir_receta_btn.Click += new System.EventHandler(this.Imprimir_receta_btn_Click);
            // 
            // return_receta_btn
            // 
            this.return_receta_btn.BackColor = System.Drawing.Color.White;
            this.return_receta_btn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("return_receta_btn.BackgroundImage")));
            this.return_receta_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.return_receta_btn.Location = new System.Drawing.Point(0, 545);
            this.return_receta_btn.Name = "return_receta_btn";
            this.return_receta_btn.Size = new System.Drawing.Size(50, 50);
            this.return_receta_btn.TabIndex = 116;
            this.return_receta_btn.UseSelectable = true;
            this.return_receta_btn.Click += new System.EventHandler(this.Return_receta_btn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Tbox_FrecUso);
            this.groupBox2.Controls.Add(this.Tbox_TieUso);
            this.groupBox2.Controls.Add(this.Tbox_Dosis);
            this.groupBox2.Controls.Add(this.Tbox_Medicamento);
            this.groupBox2.Controls.Add(this.Btn_AgregarReceta);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(22, 61);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(429, 460);
            this.groupBox2.TabIndex = 86;
            this.groupBox2.TabStop = false;
            // 
            // Tbox_FrecUso
            // 
            this.Tbox_FrecUso.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tbox_FrecUso.Location = new System.Drawing.Point(198, 330);
            this.Tbox_FrecUso.Name = "Tbox_FrecUso";
            this.Tbox_FrecUso.Size = new System.Drawing.Size(216, 28);
            this.Tbox_FrecUso.TabIndex = 118;
            // 
            // Tbox_TieUso
            // 
            this.Tbox_TieUso.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tbox_TieUso.Location = new System.Drawing.Point(198, 236);
            this.Tbox_TieUso.Name = "Tbox_TieUso";
            this.Tbox_TieUso.Size = new System.Drawing.Size(216, 28);
            this.Tbox_TieUso.TabIndex = 117;
            // 
            // Tbox_Dosis
            // 
            this.Tbox_Dosis.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tbox_Dosis.Location = new System.Drawing.Point(198, 154);
            this.Tbox_Dosis.Name = "Tbox_Dosis";
            this.Tbox_Dosis.Size = new System.Drawing.Size(216, 28);
            this.Tbox_Dosis.TabIndex = 116;
            // 
            // Tbox_Medicamento
            // 
            this.Tbox_Medicamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tbox_Medicamento.Location = new System.Drawing.Point(198, 63);
            this.Tbox_Medicamento.Name = "Tbox_Medicamento";
            this.Tbox_Medicamento.Size = new System.Drawing.Size(216, 28);
            this.Tbox_Medicamento.TabIndex = 115;
            // 
            // Btn_AgregarReceta
            // 
            this.Btn_AgregarReceta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_AgregarReceta.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.Btn_AgregarReceta.Location = new System.Drawing.Point(147, 407);
            this.Btn_AgregarReceta.Name = "Btn_AgregarReceta";
            this.Btn_AgregarReceta.Size = new System.Drawing.Size(142, 37);
            this.Btn_AgregarReceta.TabIndex = 114;
            this.Btn_AgregarReceta.Text = "Agregar";
            this.Btn_AgregarReceta.UseSelectable = true;
            this.Btn_AgregarReceta.Click += new System.EventHandler(this.Btn_AgregarReceta_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 332);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(192, 24);
            this.label15.TabIndex = 3;
            this.label15.Text = "Frecuencia de uso:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(6, 239);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(157, 24);
            this.label14.TabIndex = 2;
            this.label14.Text = "Tiempo de uso:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 157);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 24);
            this.label13.TabIndex = 1;
            this.label13.Text = "Dosis:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 67);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(142, 24);
            this.label12.TabIndex = 0;
            this.label12.Text = "Medicamento:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(371, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(186, 23);
            this.label10.TabIndex = 85;
            this.label10.Text = "RECETA MEDICA";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(-4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 23);
            this.label11.TabIndex = 84;
            this.label11.Text = "Doctor:";
            // 
            // Page_RegistroP
            // 
            this.Page_RegistroP.AutoSize = true;
            this.Page_RegistroP.Controls.Add(this.metroButton4);
            this.Page_RegistroP.Controls.Add(this.metroButton1);
            this.Page_RegistroP.Controls.Add(this.groupBox12);
            this.Page_RegistroP.Controls.Add(this.groupBox4);
            this.Page_RegistroP.Controls.Add(this.groupBox3);
            this.Page_RegistroP.Controls.Add(this.label16);
            this.Page_RegistroP.Controls.Add(this.label17);
            this.Page_RegistroP.Name = "Page_RegistroP";
            this.Page_RegistroP.Size = new System.Drawing.Size(938, 598);
            this.Page_RegistroP.Paint += new System.Windows.Forms.PaintEventHandler(this.Page_RegistroP_Paint);
            // 
            // metroButton4
            // 
            this.metroButton4.BackColor = System.Drawing.Color.White;
            this.metroButton4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButton4.BackgroundImage")));
            this.metroButton4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.metroButton4.Location = new System.Drawing.Point(0, 545);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(50, 50);
            this.metroButton4.TabIndex = 123;
            this.metroButton4.UseSelectable = true;
            // 
            // metroButton1
            // 
            this.metroButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroButton1.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton1.Location = new System.Drawing.Point(440, 524);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(142, 37);
            this.metroButton1.TabIndex = 122;
            this.metroButton1.Text = "Guardar";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.GuardarInfoPaciente);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.fechaConsulta);
            this.groupBox12.Controls.Add(this.label62);
            this.groupBox12.Location = new System.Drawing.Point(582, 367);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(284, 105);
            this.groupBox12.TabIndex = 120;
            this.groupBox12.TabStop = false;
            // 
            // fechaConsulta
            // 
            this.fechaConsulta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fechaConsulta.Location = new System.Drawing.Point(10, 59);
            this.fechaConsulta.Margin = new System.Windows.Forms.Padding(2);
            this.fechaConsulta.Name = "fechaConsulta";
            this.fechaConsulta.Size = new System.Drawing.Size(270, 24);
            this.fechaConsulta.TabIndex = 118;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(6, 16);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(189, 24);
            this.label62.TabIndex = 117;
            this.label62.Text = "Fecha de consulta:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.metroButton2);
            this.groupBox4.Controls.Add(this.Pbox_FotoP);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Location = new System.Drawing.Point(582, 63);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(310, 297);
            this.groupBox4.TabIndex = 88;
            this.groupBox4.TabStop = false;
            // 
            // metroButton2
            // 
            this.metroButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroButton2.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton2.Location = new System.Drawing.Point(96, 249);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(118, 24);
            this.metroButton2.TabIndex = 123;
            this.metroButton2.Text = "Cargar";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.ImageLoadButton_Click);
            // 
            // Pbox_FotoP
            // 
            this.Pbox_FotoP.Location = new System.Drawing.Point(56, 54);
            this.Pbox_FotoP.Name = "Pbox_FotoP";
            this.Pbox_FotoP.Size = new System.Drawing.Size(197, 174);
            this.Pbox_FotoP.TabIndex = 118;
            this.Pbox_FotoP.TabStop = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(6, 16);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(109, 24);
            this.label31.TabIndex = 117;
            this.label31.Text = "Fotografia:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbox_edadP);
            this.groupBox3.Controls.Add(this.label67);
            this.groupBox3.Controls.Add(this.Tbox_Clavepac);
            this.groupBox3.Controls.Add(this.Tbox_NombreP);
            this.groupBox3.Controls.Add(this.label66);
            this.groupBox3.Controls.Add(this.label63);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.Cbox_Doctor);
            this.groupBox3.Controls.Add(this.Tbox_TelefonoP);
            this.groupBox3.Controls.Add(this.Cbox_Sexo);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.Tbox_MailP);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.Tbox_DireccionP);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.Tbox_ApellidosP);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Location = new System.Drawing.Point(20, 27);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(502, 491);
            this.groupBox3.TabIndex = 87;
            this.groupBox3.TabStop = false;
            // 
            // tbox_edadP
            // 
            this.tbox_edadP.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbox_edadP.Location = new System.Drawing.Point(207, 305);
            this.tbox_edadP.Name = "tbox_edadP";
            this.tbox_edadP.Size = new System.Drawing.Size(252, 28);
            this.tbox_edadP.TabIndex = 140;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(221, 19);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(68, 24);
            this.label67.TabIndex = 139;
            this.label67.Text = "Clave:";
            // 
            // Tbox_Clavepac
            // 
            this.Tbox_Clavepac.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tbox_Clavepac.Location = new System.Drawing.Point(332, 19);
            this.Tbox_Clavepac.MaxLength = 100;
            this.Tbox_Clavepac.Name = "Tbox_Clavepac";
            this.Tbox_Clavepac.Size = new System.Drawing.Size(127, 28);
            this.Tbox_Clavepac.TabIndex = 138;
            // 
            // Tbox_NombreP
            // 
            this.Tbox_NombreP.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tbox_NombreP.Location = new System.Drawing.Point(207, 69);
            this.Tbox_NombreP.MaxLength = 100;
            this.Tbox_NombreP.Name = "Tbox_NombreP";
            this.Tbox_NombreP.Size = new System.Drawing.Size(252, 28);
            this.Tbox_NombreP.TabIndex = 137;
            this.Tbox_NombreP.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(27, 72);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(91, 24);
            this.label66.TabIndex = 136;
            this.label66.Text = "Nombre:";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(6, 15);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(68, 24);
            this.label63.TabIndex = 132;
            this.label63.Text = "Datos:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(27, 403);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 24);
            this.label9.TabIndex = 131;
            this.label9.Text = "Doctor:";
            // 
            // Cbox_Doctor
            // 
            this.Cbox_Doctor.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbox_Doctor.FormattingEnabled = true;
            this.Cbox_Doctor.Location = new System.Drawing.Point(207, 396);
            this.Cbox_Doctor.Name = "Cbox_Doctor";
            this.Cbox_Doctor.Size = new System.Drawing.Size(252, 30);
            this.Cbox_Doctor.TabIndex = 130;
            // 
            // Tbox_TelefonoP
            // 
            this.Tbox_TelefonoP.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tbox_TelefonoP.Location = new System.Drawing.Point(207, 151);
            this.Tbox_TelefonoP.Margin = new System.Windows.Forms.Padding(2);
            this.Tbox_TelefonoP.Mask = "(999) 000-0000";
            this.Tbox_TelefonoP.Name = "Tbox_TelefonoP";
            this.Tbox_TelefonoP.Size = new System.Drawing.Size(252, 28);
            this.Tbox_TelefonoP.TabIndex = 128;
            // 
            // Cbox_Sexo
            // 
            this.Cbox_Sexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cbox_Sexo.FormattingEnabled = true;
            this.Cbox_Sexo.Items.AddRange(new object[] {
            "Hombre",
            "Mujer"});
            this.Cbox_Sexo.Location = new System.Drawing.Point(207, 351);
            this.Cbox_Sexo.Name = "Cbox_Sexo";
            this.Cbox_Sexo.Size = new System.Drawing.Size(252, 30);
            this.Cbox_Sexo.TabIndex = 127;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(27, 357);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(64, 24);
            this.label30.TabIndex = 126;
            this.label30.Text = "Sexo:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(27, 310);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 24);
            this.label22.TabIndex = 124;
            this.label22.Text = "Edad:";
            // 
            // Tbox_MailP
            // 
            this.Tbox_MailP.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tbox_MailP.Location = new System.Drawing.Point(207, 258);
            this.Tbox_MailP.Name = "Tbox_MailP";
            this.Tbox_MailP.Size = new System.Drawing.Size(252, 28);
            this.Tbox_MailP.TabIndex = 123;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(27, 258);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 24);
            this.label21.TabIndex = 122;
            this.label21.Text = "@mail:";
            // 
            // Tbox_DireccionP
            // 
            this.Tbox_DireccionP.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tbox_DireccionP.Location = new System.Drawing.Point(207, 202);
            this.Tbox_DireccionP.Name = "Tbox_DireccionP";
            this.Tbox_DireccionP.Size = new System.Drawing.Size(252, 28);
            this.Tbox_DireccionP.TabIndex = 121;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(27, 202);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(105, 24);
            this.label20.TabIndex = 120;
            this.label20.Text = "Direccion:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(27, 155);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(99, 24);
            this.label19.TabIndex = 118;
            this.label19.Text = "Telefono:";
            // 
            // Tbox_ApellidosP
            // 
            this.Tbox_ApellidosP.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tbox_ApellidosP.Location = new System.Drawing.Point(207, 103);
            this.Tbox_ApellidosP.MaxLength = 100;
            this.Tbox_ApellidosP.Name = "Tbox_ApellidosP";
            this.Tbox_ApellidosP.Size = new System.Drawing.Size(252, 28);
            this.Tbox_ApellidosP.TabIndex = 117;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(27, 106);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(103, 24);
            this.label18.TabIndex = 116;
            this.label18.Text = "Apellidos:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(325, 1);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(286, 23);
            this.label16.TabIndex = 85;
            this.label16.Text = "REGISTRO DE PACIENTES";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(1, 1);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(168, 23);
            this.label17.TabIndex = 84;
            this.label17.Text = "Recepcionista:";
            // 
            // Page_Agenda
            // 
            this.Page_Agenda.AutoSize = true;
            this.Page_Agenda.Controls.Add(this.metroButton3);
            this.Page_Agenda.Controls.Add(this.label32);
            this.Page_Agenda.Controls.Add(this.label33);
            this.Page_Agenda.Name = "Page_Agenda";
            this.Page_Agenda.Size = new System.Drawing.Size(938, 596);
            // 
            // metroButton3
            // 
            this.metroButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroButton3.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton3.Location = new System.Drawing.Point(394, 520);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(142, 37);
            this.metroButton3.TabIndex = 123;
            this.metroButton3.Text = "Imprimir";
            this.metroButton3.UseSelectable = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(416, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(100, 23);
            this.label32.TabIndex = 87;
            this.label32.Text = "AGENDA";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(-4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(168, 23);
            this.label33.TabIndex = 86;
            this.label33.Text = "Recepcionista:";
            // 
            // Page_RegistroD
            // 
            this.Page_RegistroD.AutoSize = true;
            this.Page_RegistroD.Controls.Add(this.Btn_GuardarD);
            this.Page_RegistroD.Controls.Add(this.groupBox5);
            this.Page_RegistroD.Controls.Add(this.groupBox6);
            this.Page_RegistroD.Controls.Add(this.label34);
            this.Page_RegistroD.Controls.Add(this.label35);
            this.Page_RegistroD.Name = "Page_RegistroD";
            this.Page_RegistroD.Size = new System.Drawing.Size(938, 596);
            // 
            // Btn_GuardarD
            // 
            this.Btn_GuardarD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_GuardarD.Location = new System.Drawing.Point(437, 435);
            this.Btn_GuardarD.Name = "Btn_GuardarD";
            this.Btn_GuardarD.Size = new System.Drawing.Size(142, 37);
            this.Btn_GuardarD.TabIndex = 123;
            this.Btn_GuardarD.Text = "Guardar";
            this.Btn_GuardarD.UseSelectable = true;
            this.Btn_GuardarD.Click += new System.EventHandler(this.Btn_GuardarD_Click_1);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Btn_CargarFotoD);
            this.groupBox5.Controls.Add(this.Pbox_FotoD);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Location = new System.Drawing.Point(597, 56);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(305, 317);
            this.groupBox5.TabIndex = 122;
            this.groupBox5.TabStop = false;
            // 
            // Btn_CargarFotoD
            // 
            this.Btn_CargarFotoD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_CargarFotoD.Location = new System.Drawing.Point(95, 271);
            this.Btn_CargarFotoD.Name = "Btn_CargarFotoD";
            this.Btn_CargarFotoD.Size = new System.Drawing.Size(127, 28);
            this.Btn_CargarFotoD.TabIndex = 119;
            this.Btn_CargarFotoD.Text = "Cargar";
            this.Btn_CargarFotoD.UseSelectable = true;
            // 
            // Pbox_FotoD
            // 
            this.Pbox_FotoD.Location = new System.Drawing.Point(56, 49);
            this.Pbox_FotoD.Name = "Pbox_FotoD";
            this.Pbox_FotoD.Size = new System.Drawing.Size(200, 207);
            this.Pbox_FotoD.TabIndex = 118;
            this.Pbox_FotoD.TabStop = false;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(6, 16);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(97, 20);
            this.label36.TabIndex = 117;
            this.label36.Text = "Fotografia:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.Tbox_ClaveD);
            this.groupBox6.Controls.Add(this.label65);
            this.groupBox6.Controls.Add(this.Tbox_ContraD);
            this.groupBox6.Controls.Add(this.label51);
            this.groupBox6.Controls.Add(this.Tbox_UsuarioD);
            this.groupBox6.Controls.Add(this.label64);
            this.groupBox6.Controls.Add(this.Tbox_EspecialidadD);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Controls.Add(this.Tbox_MailD);
            this.groupBox6.Controls.Add(this.label39);
            this.groupBox6.Controls.Add(this.Tbox_DireccionD);
            this.groupBox6.Controls.Add(this.label40);
            this.groupBox6.Controls.Add(this.Tbox_TelefonoD);
            this.groupBox6.Controls.Add(this.label41);
            this.groupBox6.Controls.Add(this.Tbox_NombreD);
            this.groupBox6.Controls.Add(this.label42);
            this.groupBox6.Location = new System.Drawing.Point(29, 26);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(515, 373);
            this.groupBox6.TabIndex = 121;
            this.groupBox6.TabStop = false;
            // 
            // Tbox_ClaveD
            // 
            this.Tbox_ClaveD.Location = new System.Drawing.Point(183, 30);
            this.Tbox_ClaveD.Name = "Tbox_ClaveD";
            this.Tbox_ClaveD.Size = new System.Drawing.Size(252, 20);
            this.Tbox_ClaveD.TabIndex = 131;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(18, 30);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(58, 20);
            this.label65.TabIndex = 130;
            this.label65.Text = "Clave:";
            // 
            // Tbox_ContraD
            // 
            this.Tbox_ContraD.Location = new System.Drawing.Point(183, 327);
            this.Tbox_ContraD.Name = "Tbox_ContraD";
            this.Tbox_ContraD.Size = new System.Drawing.Size(252, 20);
            this.Tbox_ContraD.TabIndex = 129;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(18, 327);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(107, 20);
            this.label51.TabIndex = 128;
            this.label51.Text = "Contraseña:";
            // 
            // Tbox_UsuarioD
            // 
            this.Tbox_UsuarioD.Location = new System.Drawing.Point(183, 284);
            this.Tbox_UsuarioD.Name = "Tbox_UsuarioD";
            this.Tbox_UsuarioD.Size = new System.Drawing.Size(252, 20);
            this.Tbox_UsuarioD.TabIndex = 127;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(18, 284);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(76, 20);
            this.label64.TabIndex = 126;
            this.label64.Text = "Usuario:";
            // 
            // Tbox_EspecialidadD
            // 
            this.Tbox_EspecialidadD.Location = new System.Drawing.Point(183, 116);
            this.Tbox_EspecialidadD.Name = "Tbox_EspecialidadD";
            this.Tbox_EspecialidadD.Size = new System.Drawing.Size(252, 20);
            this.Tbox_EspecialidadD.TabIndex = 125;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(18, 116);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(116, 20);
            this.label38.TabIndex = 124;
            this.label38.Text = "Especialidad:";
            // 
            // Tbox_MailD
            // 
            this.Tbox_MailD.Location = new System.Drawing.Point(183, 246);
            this.Tbox_MailD.Name = "Tbox_MailD";
            this.Tbox_MailD.Size = new System.Drawing.Size(252, 20);
            this.Tbox_MailD.TabIndex = 123;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(18, 246);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(63, 20);
            this.label39.TabIndex = 122;
            this.label39.Text = "@mail:";
            // 
            // Tbox_DireccionD
            // 
            this.Tbox_DireccionD.Location = new System.Drawing.Point(183, 203);
            this.Tbox_DireccionD.Name = "Tbox_DireccionD";
            this.Tbox_DireccionD.Size = new System.Drawing.Size(252, 20);
            this.Tbox_DireccionD.TabIndex = 121;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(18, 203);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(89, 20);
            this.label40.TabIndex = 120;
            this.label40.Text = "Direccion:";
            // 
            // Tbox_TelefonoD
            // 
            this.Tbox_TelefonoD.Location = new System.Drawing.Point(183, 158);
            this.Tbox_TelefonoD.Name = "Tbox_TelefonoD";
            this.Tbox_TelefonoD.Size = new System.Drawing.Size(252, 20);
            this.Tbox_TelefonoD.TabIndex = 119;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(18, 158);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(84, 20);
            this.label41.TabIndex = 118;
            this.label41.Text = "Telefono:";
            // 
            // Tbox_NombreD
            // 
            this.Tbox_NombreD.Location = new System.Drawing.Point(183, 72);
            this.Tbox_NombreD.Name = "Tbox_NombreD";
            this.Tbox_NombreD.Size = new System.Drawing.Size(252, 20);
            this.Tbox_NombreD.TabIndex = 117;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(18, 72);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(76, 20);
            this.label42.TabIndex = 116;
            this.label42.Text = "Nombre:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(329, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(281, 23);
            this.label34.TabIndex = 89;
            this.label34.Text = "REGISTRO DE DOCTORES";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(-4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(171, 23);
            this.label35.TabIndex = 88;
            this.label35.Text = "Administrador:";
            // 
            // Page_RegistroR
            // 
            this.Page_RegistroR.AutoSize = true;
            this.Page_RegistroR.Controls.Add(this.Btn_GuardarR);
            this.Page_RegistroR.Controls.Add(this.groupBox7);
            this.Page_RegistroR.Controls.Add(this.label37);
            this.Page_RegistroR.Controls.Add(this.label43);
            this.Page_RegistroR.Controls.Add(this.groupBox8);
            this.Page_RegistroR.Name = "Page_RegistroR";
            this.Page_RegistroR.Size = new System.Drawing.Size(938, 596);
            // 
            // Btn_GuardarR
            // 
            this.Btn_GuardarR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_GuardarR.Location = new System.Drawing.Point(443, 397);
            this.Btn_GuardarR.Name = "Btn_GuardarR";
            this.Btn_GuardarR.Size = new System.Drawing.Size(127, 28);
            this.Btn_GuardarR.TabIndex = 128;
            this.Btn_GuardarR.Text = "Guardar";
            this.Btn_GuardarR.UseSelectable = true;
            this.Btn_GuardarR.Click += new System.EventHandler(this.Btn_GuardarR_Click_1);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.Btn_CargarFotoR);
            this.groupBox7.Controls.Add(this.Pbox_FotoR);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Location = new System.Drawing.Point(601, 56);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(305, 317);
            this.groupBox7.TabIndex = 127;
            this.groupBox7.TabStop = false;
            // 
            // Btn_CargarFotoR
            // 
            this.Btn_CargarFotoR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_CargarFotoR.Location = new System.Drawing.Point(95, 271);
            this.Btn_CargarFotoR.Name = "Btn_CargarFotoR";
            this.Btn_CargarFotoR.Size = new System.Drawing.Size(127, 28);
            this.Btn_CargarFotoR.TabIndex = 119;
            this.Btn_CargarFotoR.Text = "Cargar";
            this.Btn_CargarFotoR.UseSelectable = true;
            // 
            // Pbox_FotoR
            // 
            this.Pbox_FotoR.Location = new System.Drawing.Point(56, 49);
            this.Pbox_FotoR.Name = "Pbox_FotoR";
            this.Pbox_FotoR.Size = new System.Drawing.Size(200, 207);
            this.Pbox_FotoR.TabIndex = 118;
            this.Pbox_FotoR.TabStop = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 16);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(97, 20);
            this.label23.TabIndex = 117;
            this.label23.Text = "Fotografia:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(297, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(341, 23);
            this.label37.TabIndex = 125;
            this.label37.Text = "REGISTRO DE RECEPCIONISTA";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(0, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(171, 23);
            this.label43.TabIndex = 124;
            this.label43.Text = "Administrador:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label50);
            this.groupBox8.Controls.Add(this.label49);
            this.groupBox8.Controls.Add(this.Tbox_UsuarioR);
            this.groupBox8.Controls.Add(this.Tbox_ContraR);
            this.groupBox8.Controls.Add(this.label24);
            this.groupBox8.Controls.Add(this.Tbox_ClaveR);
            this.groupBox8.Controls.Add(this.Tbox_MailR);
            this.groupBox8.Controls.Add(this.label25);
            this.groupBox8.Controls.Add(this.Tbox_DireccionR);
            this.groupBox8.Controls.Add(this.label26);
            this.groupBox8.Controls.Add(this.Tbox_TelefonoR);
            this.groupBox8.Controls.Add(this.label27);
            this.groupBox8.Controls.Add(this.Tbox_NombreR);
            this.groupBox8.Controls.Add(this.label28);
            this.groupBox8.Location = new System.Drawing.Point(38, 56);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(515, 317);
            this.groupBox8.TabIndex = 126;
            this.groupBox8.TabStop = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(19, 279);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(102, 20);
            this.label50.TabIndex = 132;
            this.label50.Text = "Contraseña";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(19, 245);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(71, 20);
            this.label49.TabIndex = 131;
            this.label49.Text = "Usuario";
            // 
            // Tbox_UsuarioR
            // 
            this.Tbox_UsuarioR.Location = new System.Drawing.Point(171, 245);
            this.Tbox_UsuarioR.Name = "Tbox_UsuarioR";
            this.Tbox_UsuarioR.Size = new System.Drawing.Size(252, 20);
            this.Tbox_UsuarioR.TabIndex = 130;
            // 
            // Tbox_ContraR
            // 
            this.Tbox_ContraR.Location = new System.Drawing.Point(171, 279);
            this.Tbox_ContraR.Name = "Tbox_ContraR";
            this.Tbox_ContraR.Size = new System.Drawing.Size(252, 20);
            this.Tbox_ContraR.TabIndex = 129;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(19, 17);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(58, 20);
            this.label24.TabIndex = 128;
            this.label24.Text = "Clave:";
            // 
            // Tbox_ClaveR
            // 
            this.Tbox_ClaveR.Location = new System.Drawing.Point(170, 19);
            this.Tbox_ClaveR.Name = "Tbox_ClaveR";
            this.Tbox_ClaveR.Size = new System.Drawing.Size(252, 20);
            this.Tbox_ClaveR.TabIndex = 127;
            // 
            // Tbox_MailR
            // 
            this.Tbox_MailR.Location = new System.Drawing.Point(170, 204);
            this.Tbox_MailR.Name = "Tbox_MailR";
            this.Tbox_MailR.Size = new System.Drawing.Size(252, 20);
            this.Tbox_MailR.TabIndex = 123;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(19, 204);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 20);
            this.label25.TabIndex = 122;
            this.label25.Text = "@mail:";
            // 
            // Tbox_DireccionR
            // 
            this.Tbox_DireccionR.Location = new System.Drawing.Point(171, 168);
            this.Tbox_DireccionR.Name = "Tbox_DireccionR";
            this.Tbox_DireccionR.Size = new System.Drawing.Size(252, 20);
            this.Tbox_DireccionR.TabIndex = 121;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(19, 168);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(89, 20);
            this.label26.TabIndex = 120;
            this.label26.Text = "Direccion:";
            // 
            // Tbox_TelefonoR
            // 
            this.Tbox_TelefonoR.Location = new System.Drawing.Point(171, 114);
            this.Tbox_TelefonoR.Name = "Tbox_TelefonoR";
            this.Tbox_TelefonoR.Size = new System.Drawing.Size(252, 20);
            this.Tbox_TelefonoR.TabIndex = 119;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(19, 114);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(84, 20);
            this.label27.TabIndex = 118;
            this.label27.Text = "Telefono:";
            // 
            // Tbox_NombreR
            // 
            this.Tbox_NombreR.Location = new System.Drawing.Point(171, 65);
            this.Tbox_NombreR.Name = "Tbox_NombreR";
            this.Tbox_NombreR.Size = new System.Drawing.Size(252, 20);
            this.Tbox_NombreR.TabIndex = 117;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(19, 63);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(76, 20);
            this.label28.TabIndex = 116;
            this.label28.Text = "Nombre:";
            // 
            // navigationPage1
            // 
            this.navigationPage1.Controls.Add(this.label44);
            this.navigationPage1.Controls.Add(this.selectPatient_btn);
            this.navigationPage1.Controls.Add(this.return_patient_search_btn);
            this.navigationPage1.Controls.Add(this.label1);
            this.navigationPage1.Controls.Add(this.search_patient_txtbox);
            this.navigationPage1.Controls.Add(this.patient_table);
            this.navigationPage1.Margin = new System.Windows.Forms.Padding(2);
            this.navigationPage1.Name = "navigationPage1";
            this.navigationPage1.Size = new System.Drawing.Size(938, 596);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(211, 68);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(94, 23);
            this.label44.TabIndex = 135;
            this.label44.Text = "Nombre:";
            // 
            // selectPatient_btn
            // 
            this.selectPatient_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.selectPatient_btn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.selectPatient_btn.Location = new System.Drawing.Point(367, 504);
            this.selectPatient_btn.Name = "selectPatient_btn";
            this.selectPatient_btn.Size = new System.Drawing.Size(142, 37);
            this.selectPatient_btn.TabIndex = 134;
            this.selectPatient_btn.Text = "Seleccionar";
            this.selectPatient_btn.UseSelectable = true;
            this.selectPatient_btn.Click += new System.EventHandler(this.SelectPatient_btn_Click);
            // 
            // return_patient_search_btn
            // 
            this.return_patient_search_btn.BackColor = System.Drawing.Color.White;
            this.return_patient_search_btn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("return_patient_search_btn.BackgroundImage")));
            this.return_patient_search_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.return_patient_search_btn.Location = new System.Drawing.Point(0, 542);
            this.return_patient_search_btn.Name = "return_patient_search_btn";
            this.return_patient_search_btn.Size = new System.Drawing.Size(50, 50);
            this.return_patient_search_btn.TabIndex = 133;
            this.return_patient_search_btn.UseSelectable = true;
            this.return_patient_search_btn.Click += new System.EventHandler(this.Return_patient_search_btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(350, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(212, 23);
            this.label1.TabIndex = 131;
            this.label1.Text = "BUSCAR PACIENTE";
            // 
            // search_patient_txtbox
            // 
            this.search_patient_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search_patient_txtbox.Location = new System.Drawing.Point(305, 66);
            this.search_patient_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.search_patient_txtbox.Name = "search_patient_txtbox";
            this.search_patient_txtbox.Size = new System.Drawing.Size(369, 28);
            this.search_patient_txtbox.TabIndex = 1;
            this.search_patient_txtbox.TextChanged += new System.EventHandler(this.Search_patient_txtbox_TextChanged);
            // 
            // patient_table
            // 
            this.patient_table.AllowUserToAddRows = false;
            this.patient_table.AllowUserToDeleteRows = false;
            this.patient_table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.patient_table.Location = new System.Drawing.Point(103, 107);
            this.patient_table.Margin = new System.Windows.Forms.Padding(2);
            this.patient_table.Name = "patient_table";
            this.patient_table.RowHeadersWidth = 51;
            this.patient_table.RowTemplate.Height = 24;
            this.patient_table.Size = new System.Drawing.Size(713, 384);
            this.patient_table.TabIndex = 0;
            this.patient_table.SelectionChanged += new System.EventHandler(this.Patient_table_SelectionChanged);
            // 
            // metroContextMenu1
            // 
            this.metroContextMenu1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.metroContextMenu1.Name = "metroContextMenu1";
            this.metroContextMenu1.Size = new System.Drawing.Size(61, 4);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // printPreview
            // 
            this.printPreview.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreview.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreview.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreview.Enabled = true;
            this.printPreview.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreview.Icon")));
            this.printPreview.Name = "printPreview";
            this.printPreview.Visible = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(24, 284);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(76, 20);
            this.label45.TabIndex = 126;
            this.label45.Text = "Usuario:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(24, 327);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(107, 20);
            this.label46.TabIndex = 127;
            this.label46.Text = "Contraseña:";
            // 
            // tbox_usudoc
            // 
            this.tbox_usudoc.Location = new System.Drawing.Point(189, 286);
            this.tbox_usudoc.Name = "tbox_usudoc";
            this.tbox_usudoc.Size = new System.Drawing.Size(252, 20);
            this.tbox_usudoc.TabIndex = 128;
            // 
            // tbox_condoc
            // 
            this.tbox_condoc.Location = new System.Drawing.Point(189, 329);
            this.tbox_condoc.Name = "tbox_condoc";
            this.tbox_condoc.Size = new System.Drawing.Size(252, 20);
            this.tbox_condoc.TabIndex = 129;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(24, 27);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(58, 20);
            this.label47.TabIndex = 130;
            this.label47.Text = "Clave:";
            // 
            // tbox_clave
            // 
            this.tbox_clave.Location = new System.Drawing.Point(189, 27);
            this.tbox_clave.Name = "tbox_clave";
            this.tbox_clave.Size = new System.Drawing.Size(252, 20);
            this.tbox_clave.TabIndex = 131;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(35, 54);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(68, 24);
            this.label48.TabIndex = 133;
            this.label48.Text = "Clave:";
            // 
            // tbox_clavep
            // 
            this.tbox_clavep.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbox_clavep.Location = new System.Drawing.Point(207, 50);
            this.tbox_clavep.MaxLength = 100;
            this.tbox_clavep.Name = "tbox_clavep";
            this.tbox_clavep.Size = new System.Drawing.Size(252, 28);
            this.tbox_clavep.TabIndex = 134;
            // 
            // DGV_Expedientes
            // 
            this.DGV_Expedientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Expedientes.Location = new System.Drawing.Point(520, 78);
            this.DGV_Expedientes.Name = "DGV_Expedientes";
            this.DGV_Expedientes.RowHeadersWidth = 51;
            this.DGV_Expedientes.Size = new System.Drawing.Size(397, 307);
            this.DGV_Expedientes.TabIndex = 114;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.ClientSize = new System.Drawing.Size(962, 657);
            this.Controls.Add(this.Btn_Exit);
            this.Controls.Add(this.Lbl_SoftHospital);
            this.Controls.Add(this.NFrame_SoftHospital);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.navigationPage8.ResumeLayout(false);
            this.navigationPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exit_recepcionista)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exit_admin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_Exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NFrame_SoftHospital)).EndInit();
            this.NFrame_SoftHospital.ResumeLayout(false);
            this.NFrame_SoftHospital.PerformLayout();
            this.Page_Login.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.Page_Doctor.ResumeLayout(false);
            this.Page_Doctor.PerformLayout();
            this.Page_Recep.ResumeLayout(false);
            this.Page_Recep.PerformLayout();
            this.Page_Admin.ResumeLayout(false);
            this.Page_Admin.PerformLayout();
            this.Page_Expedientes.ResumeLayout(false);
            this.Page_Expedientes.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.navigationPage4.ResumeLayout(false);
            this.navigationPage4.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Receta)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.Page_RegistroP.ResumeLayout(false);
            this.Page_RegistroP.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Pbox_FotoP)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.Page_Agenda.ResumeLayout(false);
            this.Page_Agenda.PerformLayout();
            this.Page_RegistroD.ResumeLayout(false);
            this.Page_RegistroD.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Pbox_FotoD)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.Page_RegistroR.ResumeLayout(false);
            this.Page_RegistroR.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Pbox_FotoR)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.navigationPage1.ResumeLayout(false);
            this.navigationPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patient_table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Expedientes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void Btn_CargarFotoR_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Btn_GuardarR_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Btn_RegistroR_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion
        private DevExpress.XtraBars.Navigation.NavigationFrame NFrame_SoftHospital;
        private DevExpress.XtraBars.Navigation.NavigationPage Page_Login;
        private System.Windows.Forms.Label Lbl_SoftHospital;
        private MetroFramework.Controls.MetroTextBox Tbox_Contraseña;
        private MetroFramework.Controls.MetroTextBox Tbox_Usuario;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private MetroFramework.Controls.MetroButton Btn_Acceder;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage8;
        private MetroFramework.Controls.MetroButton metroButton18;
        private MetroFramework.Controls.MetroButton metroButton13;
        private MetroFramework.Controls.MetroButton metroButton14;
        private MetroFramework.Controls.MetroButton metroButton15;
        private MetroFramework.Controls.MetroButton metroButton16;
        private MetroFramework.Controls.MetroButton metroButton17;
        private System.Windows.Forms.Label label29;
        private DevExpress.XtraBars.Navigation.NavigationPage Page_Doctor;
        private MetroFramework.Controls.MetroButton Btn_Expediente;
        private System.Windows.Forms.PictureBox Btn_Exit;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevExpress.XtraBars.Navigation.NavigationPage Page_Recep;
        private MetroFramework.Controls.MetroButton Btn_Receta;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroButton Btn_Agenda;
        private MetroFramework.Controls.MetroButton Btn_RegistroP;
        private DevExpress.XtraBars.Navigation.NavigationPage Page_Admin;
        private MetroFramework.Controls.MetroButton Btn_RegistroR;
        private MetroFramework.Controls.MetroButton Btn_RegistroD;
        private DevExpress.XtraBars.Navigation.NavigationPage Page_Expedientes;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage4;
        private DevExpress.XtraBars.Navigation.NavigationPage Page_RegistroP;
        private DevExpress.XtraBars.Navigation.NavigationPage Page_Agenda;
        private DevExpress.XtraBars.Navigation.NavigationPage Page_RegistroD;
        private DevExpress.XtraBars.Navigation.NavigationPage Page_RegistroR;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView DGV_Receta;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private MetroFramework.Controls.MetroContextMenu metroContextMenu1;
        private System.Windows.Forms.TextBox Tbox_FrecUso;
        private System.Windows.Forms.TextBox Tbox_TieUso;
        private System.Windows.Forms.TextBox Tbox_Dosis;
        private System.Windows.Forms.TextBox Tbox_Medicamento;
        private MetroFramework.Controls.MetroButton Btn_AgregarReceta;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private MetroFramework.Controls.MetroButton return_expediente_btn;
        private MetroFramework.Controls.MetroButton return_receta_btn;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.PictureBox Pbox_FotoP;
        private System.Windows.Forms.Label label31;
        public System.Windows.Forms.ComboBox Cbox_Sexo;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label22;
        public System.Windows.Forms.TextBox Tbox_MailP;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox Tbox_DireccionP;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox Tbox_ApellidosP;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private MetroFramework.Controls.MetroButton Btn_GuardarD;
        private System.Windows.Forms.GroupBox groupBox5;
        private MetroFramework.Controls.MetroButton Btn_CargarFotoD;
        public System.Windows.Forms.PictureBox Pbox_FotoD;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox Tbox_EspecialidadD;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox Tbox_MailD;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox Tbox_DireccionD;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox Tbox_TelefonoD;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox Tbox_NombreD;
        private System.Windows.Forms.Label label42;
        private MetroFramework.Controls.MetroButton Btn_GuardarR;
        private System.Windows.Forms.GroupBox groupBox7;
        private MetroFramework.Controls.MetroButton Btn_CargarFotoR;
        public System.Windows.Forms.PictureBox Pbox_FotoR;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox Tbox_ClaveR;
        private System.Windows.Forms.TextBox Tbox_MailR;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox Tbox_DireccionR;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox Tbox_TelefonoR;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox Tbox_NombreR;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label43;
        private MetroFramework.Controls.MetroButton Btn_CargarE;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox Tbox_Observaciones;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox Tbox_Sintomas;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox Tbox_Motivo;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox Tbox_Alergias;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox Tbox_Frec;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox Tbox_Presion;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox Tbox_Temp;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox Tbox_Altura;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox Tbox_Peso;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox Tbox_Antec;
        private System.Windows.Forms.Label label52;
        public System.Windows.Forms.OpenFileDialog openFileDialog1;
        public System.Windows.Forms.MaskedTextBox Tbox_TelefonoP;
        private System.Windows.Forms.PrintPreviewDialog printPreview;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox search_patient_txtbox;
        private System.Windows.Forms.DataGridView patient_table;
        private MetroFramework.Controls.MetroButton return_patient_search_btn;
        private MetroFramework.Controls.MetroButton imprimir_receta_btn;
        private MetroFramework.Controls.MetroButton eliminar_receta_btn;
        private System.Windows.Forms.PictureBox exit_recepcionista;
        private System.Windows.Forms.PictureBox exit_admin;
        private MetroFramework.Controls.MetroButton imprimir_expediente_btn;
     //   private WindowsFormsCalendar.Calendar agenda;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.ComboBox Cbox_Doctor;
        private System.Windows.Forms.DateTimePicker fechaConsulta;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.GroupBox groupBox9;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton metroButton3;
        private System.Windows.Forms.Label label44;
        private MetroFramework.Controls.MetroButton selectPatient_btn;
        private MetroFramework.Controls.MetroButton metroButton4;
        private System.Windows.Forms.TextBox tbox_clave;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox tbox_condoc;
        private System.Windows.Forms.TextBox tbox_usudoc;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        public System.Windows.Forms.TextBox tbox_clavep;
        private System.Windows.Forms.Label label48;
        public System.Windows.Forms.TextBox tbox_nacimientoP;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox Tbox_UsuarioR;
        private System.Windows.Forms.TextBox Tbox_ContraR;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox Tbox_ClaveD;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox Tbox_ContraD;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox Tbox_UsuarioD;
        private System.Windows.Forms.Label label64;
        public System.Windows.Forms.TextBox Tbox_NombreP;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        public System.Windows.Forms.TextBox Tbox_Clavepac;
        public System.Windows.Forms.TextBox tbox_edadP;
        private System.Windows.Forms.DataGridView DGV_Expedientes;
    }
}


