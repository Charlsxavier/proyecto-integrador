Use Master
Go
If(db_id('Hospital')Is Not Null)
Drop DataBase Hospital
Create DataBase Hospital
Go

Use Hospital
Go

Create Table Doctores
(Iddoctor Int Identity Primary Key,
Llave_doc int,
Nombre_doc Varchar(20) Not Null,
Direccion_doc Varchar(25) Not Null,
Telefono_doc Varchar(15),
Especialidades_doc Varchar(20),
Correo_doc Varchar(25))
go

Create Table Pacientes
(Idpaciente Int Identity Primary Key,
Clave_pac int,
Nombre_pac Varchar(20),
Apellidos_pac Varchar(20),
Direccion_pac Varchar(25),
Telefono_pac Varchar(16),
Edad_pac Varchar(2),
Sexo_pac varchar (8),
Correo_pac Varchar(25),
Fotopaciente_pac image,
FechaConsulta_pac varchar(12),
NombreDoctor_pac Varchar(30))
go
Create Table Recepcionista
(Idrecepcionista Int Identity Primary Key,
Llave_rec int,
Nombre Varchar(25),
Direccion_rec Varchar(25),
Telefono_rec Varchar(15),
Correo_doc Varchar(25))
go

Create Table Administrador
(Idadministrador Int Identity Primary Key,
Llave_adm int,
Nombre_adm Varchar(20) Not Null)
Go

Create Table Expediente
(Idexpediente Int Identity Primary Key,
Clave_pac Int References Pacientes,
Antecedentes Varchar(50),
Peso Varchar(7),
Altura Varchar(7),
Temperatura Varchar(4),
Presion_arterial Varchar(5),
Frecuencia_cardiaca Varchar(5),
Alergias Varchar(50),
Motivodeconsulta Varchar(50),
Sintomas Varchar(50),
Observaciones Varchar(70))
go

Create Table Receta
(Idreceta Int Identity Primary Key,
Clave_pac Int References Pacientes,
Medicamento Varchar(75),
Dosis Varchar(20),
Tiempodeuso Varchar(20))
go

/* ************Usuarios***************/

Create Table Usuario
(IdUsuario Int Identity Primary Key,
Llave_doc Int References Doctores,
Llave_rec Int References Recepcionista,
Llave_adm Int References Administrador,
Acceso_Us Varchar(1) Not Null,
Usuario Varchar(20) Not Null,
Contraseņa Varchar(12) Not Null)
Go
Insert Administrador Values(1,'Carlos')
Insert Doctores Values(1,'Isacc','@Direccion','@Telefono','@Especialidad','@Correo')
Insert Recepcionista Values(1,'@Nombre','@Direccion','@Telefono','@Correo')
Insert Usuario Values(1,1,1,3,'Admin','Admin')
/***** Procedimientos Almacenados******/


/*Usuarios*/

Create Proc IniciarSesion
@Usuario Varchar(20),
@Contraseņa Varchar(12),
@VariabledeAcceso Varchar(1),
@Mensaje Varchar(50) Out
As Begin
	
	If(Not Exists(Select Usuario From Usuario Where Usuario=@Usuario))
		Set @Mensaje='Usuario o contraseņa incorrecta.'
		Else Begin
			If(Not Exists(Select Contraseņa From Usuario Where Contraseņa=@Contraseņa))
				Set @Mensaje='Usuario o contraseņa incorrecta.'
				Else Begin
				   begin
				    Set @VariabledeAcceso=(Select U.Acceso_Us From Usuario U Where U.Usuario=@Usuario)
					Select Usuario,Contraseņa From Usuario Where Usuario=@Usuario And Contraseņa=@Contraseņa
							Set @Mensaje='Bienvenido:'+@VariabledeAcceso
							end
				  End
		   End
   End
Go

/*______________________________________________________CRUDS________________________________________________________*/
Create Proc Listarpacientes
As Begin
	Select Idpaciente,Clave_pac,Nombre_pac,Apellidos_pac,Direccion_pac,Telefono_pac,
	Edad_pac,Sexo_pac,Correo_pac,Fotopaciente_pac From Pacientes 
   End
Go

Create Proc RegistrarReceta
(@Clave Int,
@Medicamento Varchar(20),
@Dosis Varchar(12),
@Tiempodeuso Varchar(12),
@Mensaje Varchar(50) Output)
As Begin
		Insert Receta Values(@Clave,@Medicamento,@Dosis,@Tiempodeuso)
		Set @Mensaje='Medicamento Registrado Correctamente.'
		End
	
Go

Create Proc BorrarMedicamentos
(@Id Int,
@Mensaje Varchar(50) Output)

As Begin
	Delete From Receta where Clave_pac=@Id
   End
Go


Create Proc Registrarpaciente
(@Clave Int,
@Nombre Varchar(20),
@Apellidos Varchar(20),
@Direccion Varchar(25),
@Telefono Varchar(16),
@Edad Varchar(2),
@Sexo Varchar (8),
@Correo Varchar(25),
@Fotopaciente image,
@FechaConsulta Varchar(12),
@NombreDoctor Varchar(30),
@Mensaje Varchar(50) Output)
As Begin
	If(Exists(Select * From Pacientes Where Clave_pac=@Clave))
		Set @Mensaje='Los Datos del Cliente ya Existen.'
	Else Begin
		Insert Pacientes Values(@Clave,@Nombre,@Apellidos,@Direccion,@Telefono,@Edad,@Sexo,
		@Correo,@Fotopaciente,@FechaConsulta,@NombreDoctor)
		Set @Mensaje='Registrado Correctamente.'
		End
	End
Go
Create Proc RegistrarDoctores
(@Clave Int,
@Nombre Varchar(20),
@Direccion Varchar(25),
@Telefono Varchar(12),
@Especialidad Varchar(20),
@Correo Varchar(25),
@Idrecepcionista int,
@Idadministrador int,
@Acceso_us Int,
@Usuario Varchar(20),
@Contraseņa Varchar(12),
@Mensaje Varchar(50) Output)
As Begin
	If(Exists(Select * From Doctores Where Llave_doc=@Clave))
		Set @Mensaje='Los Datos del Doctor ya Existen.'
	Else Begin
		Insert Doctores Values(@Clave,@Nombre,@Direccion,@Telefono,@Especialidad,@Correo)
		Insert Usuario Values(@Clave,@Idrecepcionista,@Idadministrador,@Acceso_us,@Usuario,@Contraseņa)
		Set @Mensaje='Registrado Correctamente.'
		End
	End
Go
Create Proc RegistrarRecepcionista
(@Clave Int,
@Nombre Varchar(20),
@Direccion Varchar(25),
@Telefono Varchar(12),
@Correo Varchar(25),
@Iddoctor int,
@Idadministrador int,
@Acceso_us Int,
@Usuario Varchar(20),
@Contraseņa Varchar(12),
@Mensaje Varchar(50) Output)
As Begin
	If(Exists(Select * From Recepcionista Where Llave_rec=@Clave))
		Set @Mensaje='Los Datos del Recepcionista ya Existen.'
	Else Begin
		Insert Recepcionista Values(@Clave,@Nombre,@Direccion,@Telefono,@Correo)
		Insert Usuario Values(@Clave,@Iddoctor,@Idadministrador,@Acceso_us,@Usuario,@Contraseņa)
		Set @Mensaje='Registrado Correctamente.'
		End
	End
Go


Create Proc Actualizarpaciente
(@Clave Int,
@Nombre Varchar(20),
@Apellidos Varchar(20),
@Direccion Varchar(25),
@Telefono Varchar(12),
@Edad Varchar(2),
@Sexo Char (1),
@Correo Varchar(25),
@Fotopaciente image,
@Mensaje Varchar(50) Output
)
As Begin
	If(Not Exists(Select * From Pacientes Where Clave_pac=@Clave))
		Set @Mensaje='Los Datos del Cliente no Existen.'
	Else Begin
		update Pacientes set Nombre_pac=@Nombre,Apellidos_pac=@Apellidos,Direccion_pac=@Direccion,Telefono_pac=@Telefono,
		Edad_pac=@Edad,Sexo_pac=@Sexo,Correo_pac=@Correo,Fotopaciente_pac=@Fotopaciente
		Where Clave_pac=@Clave
		Set @Mensaje='Registro actualiado Correctamente.'
		End
	End
Go

Create Proc Filtrardatospaciente
@Datos Varchar(80)
As Begin
	Select Idpaciente,Clave_pac,Nombre_pac,Apellidos_pac,Direccion_pac,Telefono_pac,
	Edad_pac,Sexo_pac,Correo_pac,Fotopaciente_pac
	From Pacientes Where Nombre_pac+' '+Apellidos_pac=@Datos or Apellidos_pac=@Datos or Nombre_pac=@Datos
End
Go

Create Proc RegistrarExp
(@Clave Int,
@Antecedentes Varchar(20),
@Peso Varchar(10),
@Altura Varchar(12),
@Temperatura Varchar(30),
@PresionArterial Varchar (8),
@FrecuenciaCardiaca Varchar(25),
@Alergias Varchar(20),
@Motivodeconsulta Varchar(25),
@Sintomas Varchar(25),
@Observaciones Varchar(16),
@Mensaje Varchar(50) Output)
As Begin
	If(Exists(Select * From Expediente Where Clave_pac=@Clave))
		Set @Mensaje='Los Datos del paciente ya Existen.'
	Else Begin
		Insert Pacientes Values(@Clave,@Antecedentes, @Peso ,@Altura ,@Temperatura ,@PresionArterial,@FrecuenciaCardiaca,@Alergias,
								@Motivodeconsulta ,@Sintomas ,@Observaciones)
			Set @Mensaje='Registrado Correctamente.'
		End
	End
Go







